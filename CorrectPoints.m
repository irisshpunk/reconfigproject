function [ correctedPointArray ] = CorrectPoints( point_array, theta, height, width )

    R = [ [cos(theta), -sin(theta)]; [sin(theta),cos(theta)]];
    correctedPointArray = zeros( size( point_array, 1 ), size( point_array, 2 ) );

    correctiveArray = [ width / 2; height / 2 ];
    for i = 1:size( point_array, 1 )
        point = [ point_array( i, 1 ); point_array( i, 2 ) ];
        correctedPoint = point - correctiveArray;
        rotatedPoint = R * ( correctedPoint );
        finalPoint = rotatedPoint + correctiveArray;
        if finalPoint(1) > width
            correctedPointArray( i, 1 ) = width;
        elseif finalPoint(1) <= 1
            correctedPointArray( i, 1 ) = 1;
        else
            correctedPointArray( i, 1 ) = uint16( finalPoint( 1 ) );
        end
        if finalPoint(2) > height
            correctedPointArray( i, 2 ) = height;
        elseif finalPoint(2) <= 1
            correctedPointArray( i, 2 ) = 1;
        else
            correctedPointArray( i, 2 ) = uint16( finalPoint( 2 ) );
        end

        point = [ point_array( i, 3 ); point_array( i, 4 ) ];
        correctedPoint = point - correctiveArray;
        rotatedPoint = R * ( correctedPoint );
        finalPoint = rotatedPoint + correctiveArray;
        if finalPoint(1) > width
            correctedPointArray( i, 3 ) = width;
        elseif finalPoint(1) <= 1
            correctedPointArray( i, 3 ) = 1;
        else
            correctedPointArray( i, 3 ) = uint16( finalPoint( 1 ) );
        end
        if finalPoint(2) > height
            correctedPointArray( i, 4 ) = height;
        elseif finalPoint(2) <= 1
            correctedPointArray( i, 4 ) = 1;
        else
            correctedPointArray( i, 4 ) = uint16( finalPoint( 2 ) );
        end
    end

end

