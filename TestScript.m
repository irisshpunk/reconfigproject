clear all;
% first load the lena image
imageColor = imread( 'Lena.jpg' );
% convert it to grayscale
imageGray = rgb2gray( imageColor );
% load the array of points
points = load( 'random_points' );
points = points.point_matrix;
% Get its descriptor
descriptor = ORB( imageGray, points );
% rotate the image
imageRotated = imrotate( imageGray, 90 );
% Get the rotated descriptor
rotatedDescriptor = ORB( imageRotated, points );
% darken the image
imageDarkened = GammaCorrect( imageGray, 0.8, 1.0 );
% get the darkened descriptor
darkenedDescriptor = ORB( imageDarkened, points );
% rotate the darkened image
imageDarkenedRotated = imrotate( imageDarkened, 90 );
% get its descriptor
darkenedRotatedDescriptor = ORB( imageDarkenedRotated, points );
% Load a non-matching image
nonMatchingImageColor = imread( 'Photographer.jpg' );
nonMatchingImageGray = rgb2gray( nonMatchingImageColor );
% Get its descriptor
nonMatchingDescriptor = ORB( nonMatchingImageGray, points );
% Compare the descriptors
sum( rotatedDescriptor == descriptor )
sum( darkenedDescriptor == descriptor )
sum( darkenedRotatedDescriptor == descriptor )
sum( nonMatchingDescriptor == descriptor )
sum( nonMatchingDescriptor == rotatedDescriptor )
sum( nonMatchingDescriptor == darkenedDescriptor )
sum( nonMatchingDescriptor == darkenedRotatedDescriptor )