-- Filename:	ORBBehavioral.vhd
--
-- This VHDL module provides a behavioral description of
-- the ORB image descriptor. The intent is to bridge the gap
-- between the MATLAB implementation and a structural model.
--
-- Authors:	Ethan Peters
--			James Coddington

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity ORB is
  generic
  (
    descriptor_length : integer := 256;
    image_size : integer := 512
  );
end entity;

architecture Behavioral of ORB is
  -- type declarations
  -- 512x512 grayscale image (1 byte per pixel)
  type image_signal is array( image_size downto 1, image_size downto 1 ) of std_logic_vector( 8 downto 1 ); 
  -- A point on the image, (x, y) in the range 1 to 512
  type image_point is array( 2 downto 1 ) of std_logic_vector( 9 downto 1 );
  -- All the points, there are 2 sets of 256 points each
  type point_array is array( 2 downto 1, descriptor_length downto 1 ) of image_point;
  -- the output type
  type image_descriptor is array( descriptor_length downto 1 ) of std_logic;

  -- these signals should eventually become ports
  -- the input image
  signal input_image : image_signal;
  -- the points for comparison
  signal selected_points : point_array;
  -- the output descriptor
  signal output_descriptor : image_descriptor;
  
  -- these signals are actually internal signals
  -- the calculated angle of the image, in whole degrees. Ranges up to 360
  signal angle : std_logic_vector( 9 downto 1 );
  -- the selected points after they've been rotated
  signal corrected_points : point_array;
  -- parameters for finding the angle of the image
  signal m_01 : std_logic_vector( 35 downto 1 ) := (others => '0');
  signal m_10 : std_logic_vector( 35 downto 1 ) := (others => '0');
  signal m_00 : std_logic_vector( 26 downto 1 ) := (others => '0');
begin

  -- this process will perform the necessary summations over the image
  summation: process( input_image )
    variable bin_i : std_logic_vector( 9 downto 1 ) := "000000001";
    variable bin_j : std_logic_vector( 9 downto 1 ) := "000000001";
  begin
      -- do 3 summations over the whole image
      for i in 1 to image_size loop
        bin_j := "000000001";
        for j in 1 to image_size loop
          m_01 <= m_01 + bin_i * input_image( i, j );
          m_10 <= m_10 + bin_j * input_image( i, j );
          m_00 <= m_00 + input_image( i, j );
          bin_j := bin_j + "0001";
        end loop;
        bin_i := bin_i + "0001";
      end loop;
      -- adjust m_10 and m_10
      m_10 <= std_logic_vector( unsigned( m_10 ) / unsigned( m_00 ) ) -
            std_logic_vector(to_unsigned(image_size/2,16));
      m_01 <= std_logic_vector( unsigned( m_01 ) / unsigned( m_00 ) ) -
            std_logic_vector(to_unsigned(image_size/2,16));
      -- get the arctan2
  end process summation;
  
  -- this process will actually calculate the angle of the image
  -- TODO:
  angle_calculation: process( m_10, m_01 )
  begin
  end process angle_calculation;
  
  -- this process will rotate the selected points
  point_correction: process( angle )
    variable sine : std_logic_vector( 9 downto 1 ) := (others => '0');
    variable cosine : std_logic_vector( 9 downto 1 ) := (others => '0');
    variable shifted_x : std_logic_vector( 9 downto 1 );
    variable shifted_y : std_logic_vector( 9 downto 1 );
    variable temp_x : std_logic_vector( 27 downto 1 );
    variable temp_y : std_logic_vector( 27 downto 1 );
  begin
    -- TODO:
    -- calculate the sine and cosine of the angle,
    -- both will be less than 1
    -- for each point in the array
    for i in 1 to descriptor_length loop
      -- calculate new values of x and y
      -- correct the points to be around the origin
      shifted_x := selected_points( 1, i )( 1 ) - 
          std_logic_vector(to_unsigned(image_size/2,9));
      shifted_y := selected_points( 1, i )( 2 ) - 
          std_logic_vector(to_unsigned(image_size/2,9));
      -- we implement the sine thing as a fixed point multiplication
      temp_x := (shifted_x & "000000000") * cosine -
                (shifted_y & "000000000") * sine;
      temp_y := (shifted_x & "000000000") * sine +
                (shifted_y & "000000000") * cosine;
      -- so we have a fixed point result in a 18.9 arrangement,
      -- and we need to pull out the middle 9 and shift back
      -- to the center of the image
      shifted_x := temp_x( 18 downto 10 ) + 
          std_logic_vector(to_unsigned(image_size/2,9));
      shifted_y := temp_y( 18 downto 10 ) + 
          std_logic_vector(to_unsigned(image_size/2,9));
      -- now we just have to make sure that nothing is out of bounds
      if shifted_x > std_logic_vector(to_unsigned(image_size,9)) then
        corrected_points( 1, i )( 1 ) <= std_logic_vector(to_unsigned(image_size,9));
      elsif shifted_x < "000000001" then
        corrected_points( 1, i )( 1 ) <= "000000001";
      else
        corrected_points( 1, i )( 1 ) <= shifted_x;
      end if;
      if shifted_y > std_logic_vector(to_unsigned(image_size,9)) then
        corrected_points( 1, i )( 2 ) <= std_logic_vector(to_unsigned(image_size,9));
      elsif shifted_y < "000000001" then
        corrected_points( 1, i )( 2 ) <= "000000001";
      else
        corrected_points( 1, i )( 2 ) <= shifted_y;
      end if;
      -- correct the points to be around the origin
      shifted_x := selected_points( 2, i )( 1 ) - 
          std_logic_vector(to_unsigned(image_size/2,9));
      shifted_y := selected_points( 2, i )( 2 ) - 
          std_logic_vector(to_unsigned(image_size/2,9));
      -- we implement the sine thing as a fixed point multiplication
      temp_x := (shifted_x & "000000000") * cosine -
                (shifted_y & "000000000") * sine;
      temp_y := (shifted_x & "000000000") * sine +
                (shifted_y & "000000000") * cosine;
      -- so we have a fixed point result in a 18.9 arrangement,
      -- and we need to pull out the middle 9 and shift back
      -- to the center of the image
      shifted_x := temp_x( 18 downto 10 ) + 
          std_logic_vector(to_unsigned(image_size/2,9));
      shifted_y := temp_y( 18 downto 10 ) + 
          std_logic_vector(to_unsigned(image_size/2,9));
      -- now we just have to make sure that nothing is out of bounds
      if shifted_x > std_logic_vector(to_unsigned(image_size,9)) then
        corrected_points( 2, i )( 1 ) <= std_logic_vector(to_unsigned(image_size,9));
      elsif shifted_x < "000000001" then
        corrected_points( 2, i )( 1 ) <= "000000001";
      else
        corrected_points( 2, i )( 1 ) <= shifted_x;
      end if;
      if shifted_y > std_logic_vector(to_unsigned(image_size,9)) then
        corrected_points( 2, i )( 2 ) <= std_logic_vector(to_unsigned(image_size,9));
      elsif shifted_y < "000000001" then
        corrected_points( 2, i )( 2 ) <= "000000001";
      else
        corrected_points( 2, i )( 2 ) <= shifted_y;
      end if;
    end loop;
  end process point_correction;

  -- this process will extract the corrected points out
  -- of the image, compare their intensities and form
  -- the descriptor
  description_calculation: process( corrected_points )
    variable temp1 : std_logic_vector( 8 downto 1 );
    variable temp2 : std_logic_vector( 8 downto 1 );
    variable x1 : std_logic_vector( 9 downto 1 );
    variable y1 : std_logic_vector( 9 downto 1 );
    variable x2 : std_logic_vector( 9 downto 1 );
    variable y2 : std_logic_vector( 9 downto 1 );
  begin
      for i in 1 to descriptor_length loop
          x1 := corrected_points( 1, i )( 1 );
          y1 := corrected_points( 1, i )( 2 );
          x2 := corrected_points( 2, i )( 1 );
          y2 := corrected_points( 2, i )( 2 );
          temp1 := input_image( to_integer(unsigned(x1)), 
                                to_integer(unsigned(y1)) );
          temp2 := input_image( to_integer(unsigned(x2)),
                                to_integer(unsigned(y2)) );
          if ( temp1 < temp2 ) then
            output_descriptor( i ) <= '1';
          else
            output_descriptor( i ) <= '0';
          end if;
      end loop;
  end process description_calculation;

end architecture Behavioral;
