% Read in the image
%input_img = rgb2gray(imread('../reconfigproject/Lena.jpg'));
input_img = imrotate(rgb2gray(imread('../reconfigproject/Lena.jpg')), 90);
%result_img = rgb2gray(imread('../reconfigproject/Lena.jpg'));
result_img = imrotate(rgb2gray(imread('../reconfigproject/Lena.jpg')), 90);


% Clear the output image
for i = 1:size(result_img, 1)
    for j = 1:size(result_img, 2)
        result_img(i, j) = 0;
    end
end

% Display the input image
figure
imshow(input_img)

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % FPGA
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Clear any existing serial ports
delete(instrfind);
% Create a serial port object
serialPort = serial('COM23','BaudRate',115207,'InputBufferSize',65536,'OutputBufferSize',8192);
% open the serial port for reading/writing
fopen(serialPort);

tic
% Tell the FPGA an image is coming
fwrite(serialPort, 1, 'uint8');

%pause(100);

% Determine the rows, columns, and image size
rows = uint16(size(input_img, 1))
cols = uint16(size(input_img, 2))
img_size = uint32(uint32(rows) * uint32(cols))

% % Tell the FPGA the number of rows and columns and the image size
fwrite(serialPort, rows, 'uint16')
fwrite(serialPort, cols, 'uint16')
fwrite(serialPort, img_size, 'uint32')

% Send the image to the FPGA
for i = 1:size(input_img, 1)
	for j = 1:size(input_img, 2)
        fwrite(serialPort, input_img(i, j), 'uint8');
	end
end

fpga_transmit_time = toc

%pause(1);

% Tell the FPGA to run the descriptor
fwrite(serialPort, 2, 'uint8')

%pause(1);

% Get the number of cycles that the median filter ran
% cycle_cnt = fread(serialPort, 1, 'uint32')

% fpga_proc_time = cycle_cnt / 100000000

tic
% Tell the FPGA to send the descriptor back
fwrite(serialPort, 3, 'uint8')

% Read the image back from the FPGA
for i = 1:32
    byte(i) = fread(serialPort, 1, 'uint8')
end

descriptor = [];

for i = 1:32
    for j = 1:8
        if ( 0 == mod( byte(i), 2 ) )
            descriptor = [ descriptor 0 ];
        else
            descriptor = [ descriptor 1 ];
        end
        byte(i) = uint8(floor(byte(i) / 2));
    end 
end

fpga_receive_time = toc

% Display the filtered image
% figure
% imshow(result_img)

% Close the serial port
fclose(serialPort)
delete(serialPort)
clear serialPort