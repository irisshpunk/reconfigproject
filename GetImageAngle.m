function [ angle, m_10, m_01 ] = GetImageAngle( input_image )
% GETIMAGEANGLE Determines the angle of an image
%   It's an intensity image

    % Follow the summation: m_pq = Sum(x^p * y^q * I(x, y))
    %   for pq = 10, 01
    m_01 = 0;
    m_10 = 0;
    for i = 1:size( input_image, 1 )
        for j = 1:size( input_image, 2 )
            %m_01 = m_01 +  double(j^0) * double(i^1) * double(input_image( i, j ));
            % for a 512x512, 8 bit grayscale image, this maxes out 
            % at 1.7213e+010, which fits in 35  bits
            m_01 = m_01 + double(i) * double(input_image( i, j ));
            %m_10 = m_10 + double(j^1) * double(i^0) * double(input_image( i, j ));
            % for a 512x512, 8 bit grayscale image, this maxes out 
            % at 1.7213e+010, which fits in 35  bits
            m_10 = m_10 + double(j) * double(input_image( i, j ));
        end
    end
    % Find theta = atan2(m_01, m_10)
    % for a 512x512, 8 bit grayscale image, this maxes out at 67108864
    % which fits in 26 unsigned bits
    t = sum(sum(input_image));
    m_00 = t;
    m_10;
    m_01;
    % convert the sums into a point and move to the origin
    m_10 = (m_10/t) - (size(input_image, 2)/2);
    m_01 = (m_01/t) - (size(input_image, 1)/2);
    angle = atan2( m_01, m_10 );

end

