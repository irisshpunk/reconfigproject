\documentclass[12pt]{article}
\usepackage{amsmath}
\title{ORB Coprocessor}
\author{Ethan Peters \\ James Coddington}
\date{}
\begin{document}
	\maketitle
	\section{Abstract}
	{
		In this work, a high performance design for the ORB image descriptor is presented and evaluated. The goals of this work
		are twofold: first, it is hoped that this particular algorithm can be implemented correctly and at improved performance.
		Secondly, it is hoped that this work will prove the viability and utility of ASICs for image description. The design
		presented is viable and can be verified in simulation, though constraints in both time and equipment conspire to doom
		the hardware implementation. Ultimately, the hardware implementation fails, but valuable information about the limitations
		and prospects of the approach are obtained.
	}
	\section{Algorithm Introduction}
	{
		An image descriptor is a method for representing an image in a way that is compact, unique and relatively cheap to compute.
		Image descriptors have many implementations, such as object tracking, image stitching, image registration and stereo imaging;
		consequently, it is of great benefit to be able to generate image descriptions quickly enough so that these applications can
		be done in real time. Several families of descriptors have been designed with the primary purpose of reducing computation
		time. The algorithm used as the focus of this project is the Oriented and Rotated BRIEF (ORB) image descriptor, a variation
		on the BRIEF image descriptor. The BRIEF image descriptor begins with a set of N random points, and collects them into pairs;
		for each pair, the pixel values of the points are extracted from the image and subtracted. If the result is positive, a
		corresponding bit in a bit string is set, otherwise it is cleared, resulting in a bit string of length N describing the image.
		It can be seen that this descriptor is computationally efficient, and in fact the descriptions are relatively ``good'', meaning
		that they are sufficiently close to being unique to an object, and are relatively repeatable given a well posed data set.
		However, this descriptor can be ``tricked''; that is, given two images of the same object, the descriptor will not show strong
		correlation under certain circumstances. Specifically, if an object is rotated in plane, its BRIEF description will change.
		To overcome this, the ORB algorithm was implemented; ORB uses the same set of random pairs concept used in BRIEF, but before the
		points are compared they are rotated to compensate for the image's rotation. The orientation of the image is determined using
		the intensity centroid, calculated as follows:
		\begin{equation*}
		{
			m_{pq} = \sum \sum I(x,y) x^p y^q
		}
		\end{equation*}
		\begin{equation*}
		{
			\theta = tan^{-1} \left( \frac{m_{10}}{m_{00}}, \frac{m_{01}}{m_{00}} \right)
		}
		\end{equation*}
		Once the angle \begin{math}{\theta}\end{math} is determined, each point is rotated by that amount, then the pixel values are
		extracted and compared. This makes ORB rotationally invariant, as well as having inherited its invariance to global illumination
		changes from BRIEF. The resulting algorithm has a high degree of parallelism, making it suitable for hardware acceleration;
		furthermore, each core is performing an extremely simple operation:
		\begin{equation*}
		{
			x' = x \times cos( \theta ) - y \times sin( \theta )
		}
		\end{equation*}
		\begin{equation*}
		{
			y' = x \times sin( \theta ) + y \times cos( \theta )
		}
		\end{equation*}
		making the algorithm suitable for implementation on an FPGA.
	}
	\section{Hardware Design}
	{
		Several caveats had to be made in order to realize the ORB algorithm in hardware. First, the sin, cosine and arctangent
		of an angle are not readily calculable in hardware; the equations were modified as follows:
		\begin{equation*}
		{
			x_c = \frac{m_{10}}{m_{00}}, y_c = \frac{m_{01}}{m_{00}}
		}
		\end{equation*}
		\begin{equation*}
		{
			C = ( x_c^2 + y_c^2 )^{-1/2}
		}
		\end{equation*}
		\begin{equation*}
		{
			\hat{x} = \frac{x_c}{C}, \hat{y} = \frac{y_c}{C}
		}
		\end{equation*}
		We can treat the new, normalized centroid as a phasor, with radius 1 and angle \begin{math}{ \theta }\end{math}. Multiplying by this
		phasor will result in a rotation through \begin{math}{ \theta }\end{math} and a scaling of 1.
		\begin{equation*}
		{
			x' + iy' = (x + iy)(\hat{x} + i\hat{y}) = x\hat{x} + (-1)y\hat{y} + xi\hat{y} + yi\hat{x} = (x\hat{x} - y\hat{y}) + i(x\hat{y} + y\hat{x})
		}
		\end{equation*}
		\begin{equation*}
		{
			x' = x\hat{x} - y\hat{y}, y' = x\hat{y} + y\hat{x}
		}
		\end{equation*}
		Basic trigonometry tells us this is true because:
		\begin{equation*}
		{
			cos( \theta ) = \hat{x} = \frac{x_c}{C}, sin( \theta ) = \hat{y} = \frac{y_c}{C}
		}
		\end{equation*}
		where C is the circle's radius. We can calculate the magnitude of the vector using CORDIC simply enough, and we can use CORDIC to perform
		normalization of the vector because the result of the division is less than or equal to 1. Dividing the image moments to get the components
		of the centroid requires the use of a first class dividor, which is acquired using Xilinx IP core generator.
		\newline
		The second consideration is that not all points can be processed simultaneously, because the standard ORB implementation considers 512
		points, each of which has two components; each component has to be multiplied, requiring a DSP slice, and given hardware limitations with
		the FPGA used, we expected to be able to process between 10 and 30 point rotations simultaneously. Consequently, the system had to be designed
		such that it would process groups of points (of variable size) simultaneously until it had covered the entire image.
		\newline
		Being a coprocessor design, the system was to communicate with a primary processor that would transmit the image and expect to receive the
		image's description. This led to the setup shown in Figure 1 (overall architecture borrowed from lab assignments 5 and 6), with all novel
		work being contained in the module labeled "Coprocessor". A bottleneck immediately makes itself apparent, in that the image (which is a substantive
		512x512 resolution) has to be transmitted over a serial UART connection; to help alleviate some of this performance detriment, the image moment
		calculation is performed while the image is being received so that at the end of the image's transmission \begin{math} { m_{10}, m_{01}}\end{math}
		and \begin{math}{ m_{00} }\end{math} (called the image moments) are already known.
		\newline
		Figure 2 shows the architecture of the coprocessor module; the Summator module is the one responsible for calculating the image moments, the Wide
		Point Rotator calculates the image centroid phasor and rotates all of the points, the Point Extractor gets the pixel intensities out of memory,
		the Wide Point Comparator generates the descriptor from pixel values and the Serializer is responsible for turning the 256 bit descriptor into
		32 distinct bytes in sequence.
		\newline
		A second bottleneck is the retrieval of pixel data from SRAM after a point is rotated. To help alleviate this, point extraction is set up to operate in
		groups and pull data from a FIFO (labeled the Wide FIFO), so that a group of points can begin to be pulled from memory while the next group is being rotated.
		Figure 3 shows the internal structure of the Wide Point Rotator; note that the chain of stages in the upper part of the diagram are the "most serial" part
		of the algorithm, and the lower portion of the diagram is where a large portion of the inherent parallelism is expressed, making this the most significant
		component in the design, as well as one of the more resource demanding components.
		\newline
		The Normalizer is a fancy name for the CORDIC dividor, used to normalize the centroid vector. The Magnitude Calculator is a module which implements the
		standard CORDIC algorithm for magnitude calculation for a vector. The Centroid Calculator uses the IP core dividor to calculate the centroid vector
		from the image moments.
		\newline
		Figure 4 shows the Point Extractor's internal design; note that this stage is also responsible for setting up the data in parallel form for the next stage,
		which is the Wide Point Comparator. Shown in Figure 5, the Wide Point Comparator exploits the other highly parallel stage of the process, which generates
		the actual image description. Not pictured are the Serializer's internal components (which are not the focus of this project) or the single point rotator's
		internals (which are convoluted due to optimization, but accomplish the function already described).
	}
	\section{Implementation Results}
	{
		There were several challenges to implementing the coprocessor, even after the design had been reasoned out and verified in simulation. The design occupied
		an enormous amount of the chip when put onto the FPGA, and the point rotation module had to be reduced to processing only 4 points simultaneously
		in order to be successfully routed. At this point, the design would not meet timing requirements, so pipeline stages were added to all long datapaths in
		order to meet timing requirements, pushing chip usage right to the edge. Finally, it was determined that a critical component of the Magnitude Calculator
		could not be fit onto the chip with the rest of the design. Specifically, the Magnitude Calculator needs to scale the output of the CORDIC algorithm by
		some corrective factor; the corrective factor depends on the number of iterations (though it converges to a specific, calculable value). A LUT was to be
		used to calculate this value for small numbers of iterations, but very near the end of the allotted time for this project it was discovered that this
		LUT could not fit, and thus the Coprocessor could not calculate a correct descriptor for some images (unfortunately, including the most popular and
		useful test images). However, it was still the case that the calculated image descriptor was consistent for a given image, and distinct from the descriptors
		for other images.
		\newline
		Figure 6 shows pairings of test images and their descriptors (calculated using the coprocessor and a baseline MATLAB script).
		\newline
		Additionally, it was found that, not only was transmission of the image over UART far too slow to be beneficial (upwards of 4 minutes), SRAM accesses 
		consume way more than the
		accepted time budget for generating an ORB descriptor (known from other projects to be 16 microseconds for a semi-optimized ORB descriptor). Even in a
		simulation where the baud rate and SRAM accesses were made unrealistically fast, it took 25 seconds to calculate a single descriptor. It is worth noting
		that the coprocessor's run time can not be compared fairly to the MATLAB code since the MATLAB implementation ran so quickly it could not be accurately
		measured using MATLAB tools.
	}
	\section{Conclusion}
	{
		In practice, the work presented fails in its intended goal (though, given that high performance implementations of imaging algorithms is an open field of
		research, and this work has not been attempted, the project can not truly fail), it does raise hope. In particular, this design worked in simulation and
		was only prevented from working properly in hardware by limitations in the device used and time allotted. Consequently, it stands to reason that, given
		a larger chip, the design could be made to work. However, it also poses serious drawbacks to consider when trying to implement image descriptors in hardware;
		first, for such a coprocessor to be worthwhile, it would have to be in such a system where transmission times were entirely negligible and memory access
		was extremely rapid, or alternatively a system where the primary processor was extremely slow or overloaded. Such a system is actually not hard to imagine
		or build; consider that the SRAM used in this system takes 70 nanoseconds to access, yielding an equivalent memory clock of \textasciitilde 14 MHz, of which we use one
		edge. Compare this to a DDR3 system, which uses both edges of a \textasciitilde 1 GHz clock. Additionally, the UART module used had a baud rate of 115200, meaning a bit
		rate of 115.2 Kb/s or a byte rate of around 11.52 KB/s; compare this to something like a PCIe v4 slot (typical for high end coprocessors in desktop computers),
		which has a theoretical maximum byte rate of \textasciitilde 2 GB/s. These factors imply that an image description coprocessor could feasibly present a performance
		boost, were it build with sufficient chip area and integrated into a sufficiently advanced system. One final note worth making is that one would typically
		consider such a system to be extremely excessive simply to perform image description; however, such a coprocessor could be integrated into a standard (discrete)
		graphics card and see the kind of tech specs it requires to be worthwhile (though one could argue that it would be better to just program the GPU to perform
		the task at hand, but more research is needed to weigh that decision).
	}
	\section{Appendices}
	{
	}
\end{document}


