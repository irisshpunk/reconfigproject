function [ image_descriptor ] = ORB( input_image, point_array )
%ORB This function implements the ORB image descriptor
%   That's really about it
%   The input image is an intensity image
%   The point array is an array of [(x,y) (x,y)] pairs


	tic;
    % First, determine the angle of the image
    [theta, m_10, m_01] = GetImageAngle( input_image );
    %theta = -theta;
    % Rotate to correct
    %correctedImage = imrotate( input_image, theta );
    height = size( input_image, 1 );
    width = size( input_image, 2 );
    correctedPointArray = CorrectPoints( point_array, theta, height, width );
    % debugging
    % show the image
    %figure;
    %imshow( input_image );
    %hold on;
    % draw the center
    %plot( 256, 256, 'g.' );
    %plot( m_10 + 256, m_01 + 256, 'b.' );
    %for i = 1:size( correctedPointArray, 1 )
     %   plot( correctedPointArray(i, 2), correctedPointArray(i, 1), 'r.' );
      %  plot( correctedPointArray(i, 4), correctedPointArray(i, 3), 'r.' );
    %end
    % draw points on it
    % Grab all the points in the array
    intensities = zeros( size( correctedPointArray, 1 ), 2 );
    for i = 1:size( correctedPointArray, 1 )
        intensities(i, 1) = input_image( correctedPointArray(i, 2), correctedPointArray(i, 1) );
        intensities(i, 2) = input_image( correctedPointArray(i, 4), correctedPointArray(i, 3) );
    end
    % For each pair of points
    image_descriptor = zeros( 1, 256 );
    %image_descriptor = zeros( 1, 32 );
    for i = 1:size( intensities, 1 )
    %i = 1;
    %for j = 1:32
    %    for k = 1:8
            % compare them
            if intensities( i, 1 ) < intensities( i, 2 )
              % add to the bitstring output
                image_descriptor(i) = 1;
                %image_descriptor( j ) = image_descriptor( j ) + 2^(k-1);
            end
            i = i + 1;
    %    end
    end

	g = toc

end

