function [ output_image ] = GammaCorrect( input_image, gamma, coeff )

    output_image = uint8( coeff * double(input_image) .^ gamma );

end

