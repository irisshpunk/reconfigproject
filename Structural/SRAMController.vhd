----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:12:32 10/28/2013 
-- Design Name: 
-- Module Name:    SRAMController - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SRAMController is
	PORT (
		CLOCK : in std_logic;
		RESET : in std_logic;
		DATA_IN : in std_logic_vector( 8 downto 1 );
		DATA_OUT : out std_logic_vector( 8 downto 1 );
		C : out std_logic_vector( 8 downto 1 );
		SRAM_WRITE : in std_logic;
		SRAM_READ : in std_logic;
		READ_FINISHED : out std_logic;
		WRITE_FINISHED : out std_logic;
		ADDRESS : in std_logic_vector( 22 downto 0 );
		CE_N : out std_logic;
		WE_N : out std_logic;
		OE_N : out std_logic;
		ADV_N : out std_logic;
		CRE : out std_logic;
		DQ : inout std_logic_vector( 15 downto 0 );
		LB_N : out std_logic;
		UB_N : out std_logic;
		busy : in std_logic;
		OUT_ADDRESS : out std_logic_vector( 22 downto 0 )
	);
end SRAMController;

architecture Behavioral of SRAMController is

	type state is ( IDLE, READ_DATA, WE_HIGH, LATCH_READ_ADDRESS, WAIT_1, OE_LOW, WAIT_2, OUTPUT_VALID, HOLD,
						 WRITE_DATA, LATCH_INPUT, WE_LOW, WAIT_3, LATCH_WRITE_ADDRESS, WAIT_4, WAIT_5 );
	signal current_state : state := IDLE;
	signal counter : std_logic_vector( 32 downto 1 ) := (others => '0');
	signal latched_input : std_logic_vector( 8 downto 1 );

begin

	ADV_N <= '0';
	CE_N <= '0';
	LB_N <= '0';
	UB_N <= '0';
	CRE <= '0';

--	C <= "00000000";

--	C <= DQ( 7 downto 0 );

--	DATA_OUT <= DQ( 7 downto 0 );

	process( CLOCK, RESET )
	begin
		if ( CLOCK'event and CLOCK = '1' ) then
		if ( RESET = '1' ) then
			current_state <= IDLE;
			C <= "00000000";
		else
			case current_state is
				when IDLE =>
					WE_N <= '1';
					if ( SRAM_WRITE = '1' ) then
						current_state <= WRITE_DATA;
					elsif ( SRAM_READ = '1' ) then
						current_state <= READ_DATA;
					else
						current_state <= IDLE;
					end if;
					C <= SRAM_WRITE & "0000000";
				when READ_DATA =>
					current_state <= WE_HIGH;
					C <= SRAM_WRITE & "0000001";
				when WE_HIGH =>
					current_state <= LATCH_READ_ADDRESS;
					C <= SRAM_WRITE & "0000010";
				when LATCH_READ_ADDRESS =>
					OUT_ADDRESS <= ADDRESS;
					current_state <= WAIT_1;
					C <= SRAM_WRITE & "0000011";
				when WAIT_1 =>
					DQ <= (others => 'Z');
					if ( unsigned(counter) = 14 ) then
						counter <= (others => '0');
						current_state <= OUTPUT_VALID;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
					end if;
					C <= SRAM_WRITE & "0000100";
				when OE_LOW =>
				when WAIT_2 =>
				when OUTPUT_VALID =>
					DATA_OUT <= DQ( 7 downto 0 );
					current_state <= WAIT_3;
					C <= SRAM_WRITE & "0000101";
				when HOLD =>
					if ( unsigned(counter) = 14 ) then
						counter <= (others => '0');
						current_state <= IDLE;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
					end if;
					C <= SRAM_WRITE & "0000110";
				when WRITE_DATA =>
					current_state <= LATCH_INPUT;
				when LATCH_INPUT =>
--					latched_input <= DATA_IN;
					DQ <= "00000000" & DATA_IN;
					OUT_ADDRESS <= ADDRESS;
--					WE_N <= '0';
--					current_state <= WAIT_4;
					current_state <= WE_LOW;
					C <= SRAM_WRITE & "0000111";
				when WE_LOW =>
					WE_N <= '0';
					current_state <= WAIT_4;
				when WAIT_3 =>
					current_state <= IDLE;
				when LATCH_WRITE_ADDRESS =>
				when WAIT_4 =>
					if ( unsigned(counter) = 14 ) then
						counter <= (others => '0');
						current_state <= WAIT_5;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
						current_state <= WAIT_4;
					end if;
					C <= SRAM_WRITE & "0001000";
				when WAIT_5 =>
					WE_N <= '1';
					if ( unsigned(counter) = 1 ) then
						counter <= (others => '0');
						current_state <= IDLE;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
						current_state <= WAIT_5;
					end if;
					C <= SRAM_WRITE & "0001001";
--				when others =>
--					current_state <= IDLE;
			end case;
		end if;
		end if;
	end process;

	process ( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '0' ) then
		case current_state is
			when IDLE =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '1';
			when READ_DATA =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '1';
			when WE_HIGH =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '1';
			when LATCH_READ_ADDRESS =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '0';
			when WAIT_1 =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '0';
			when OUTPUT_VALID =>
				READ_FINISHED <= '1';
				WRITE_FINISHED <= '0';
				OE_N <= '0';
			when WRITE_DATA =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '1';
			when LATCH_INPUT =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '1';
			when WAIT_4 =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '1';
			when WAIT_5 =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '1';
				OE_N <= '1';
			when WAIT_3 =>
				READ_FINISHED <= '0';
				WRITE_FINISHED <= '0';
				OE_N <= '0';
			when others =>
		end case;
		end if;
	end process;

end Behavioral;

