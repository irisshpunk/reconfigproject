library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity CentroidCalculator is
	port(
		m_10 : in std_logic_vector( 64 downto 1 );
		m_01 : in std_logic_vector( 64 downto 1 );
		m_00 : in std_logic_vector( 32 downto 1 );
		width : in std_logic_vector( 16 downto 1 );
		height : in std_logic_vector( 16 downto 1 );
		START : in std_logic;
		CLOCK : in std_logic;
		FINISHED : out std_logic;
		x_0 : out std_logic_vector( 32 downto 1 );
		y_0 : out std_logic_vector( 32 downto 1 )
	);
end entity;


architecture Behavioral of CentroidCalculator is

COMPONENT IP_DIVIDOR
  PORT (
    aclk : IN STD_LOGIC;
    s_axis_divisor_tvalid : IN STD_LOGIC;
    s_axis_divisor_tready : OUT STD_LOGIC;
    s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axis_dividend_tvalid : IN STD_LOGIC;
    s_axis_dividend_tready : OUT STD_LOGIC;
    s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axis_dout_tvalid : OUT STD_LOGIC;
    m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
  );
END COMPONENT;

-- type declarations
	type state is ( IDLE, X_CALC, Y_CALC, DONE, WAIT_STATE );
	signal current_state : state := IDLE;

-- signal declarations
	signal mux_out : std_logic_vector( 64 downto 1 );
	signal mux_select : std_logic := '0';
	signal div_out : std_logic_vector( 64 downto 1 );
	signal unshifted_x : std_logic_vector( 32 downto 1 );
	signal unshifted_y : std_logic_vector( 32 downto 1 );
	signal counter : std_logic_vector( 10 downto 1 ) := ( others => '0' );

	-- div control
	signal s_axis_divisor_tvalid : std_logic := '0';
	signal s_axis_dividend_tvalid : std_logic := '0';
	signal div_output_ready : std_logic;

	signal x_en : std_logic;
	signal y_en : std_logic;

begin

	-- component instantations
	div : IP_DIVIDOR
  PORT MAP (
    aclk => CLOCK,
    s_axis_divisor_tvalid => s_axis_divisor_tvalid,
--    s_axis_divisor_tready => s_axis_divisor_tready,
    s_axis_divisor_tdata => m_00,
    s_axis_dividend_tvalid => s_axis_dividend_tvalid,
--    s_axis_dividend_tready => s_axis_dividend_tready,
    s_axis_dividend_tdata => mux_out,
    m_axis_dout_tvalid => div_output_ready,
    m_axis_dout_tdata => div_out
  );
  
	-- concurrent signal assignments
	x_0 <= std_logic_vector( unsigned( unshifted_x ) - unsigned( '0' & width( 16 downto 2 ) ) );
	y_0 <= std_logic_vector( unsigned( unshifted_y ) - unsigned( '0' & height( 16 downto 2 ) ) );
--	div_out <= std_logic_vector( unsigned( mux_out ) / unsigned( m_00 ) );
	with mux_select select
		mux_out <= m_10 when '0',
			   m_01 when '1',
			   (others => '0') when others;

	-- process blocks
	-- control
	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			case current_state is
				when IDLE =>
					counter <= ( others => '0' );
					if ( START = '1' ) then
						current_state <= X_CALC;
						s_axis_divisor_tvalid <= '1';
						s_axis_dividend_tvalid <= '1';
					else
						current_state <= IDLE;
						s_axis_divisor_tvalid <= '0';
						s_axis_dividend_tvalid <= '0';
					end if;
				when X_CALC =>
--					if ( unsigned(counter) = 10 ) then
					if ( div_output_ready = '1' ) then
						counter <= ( others => '0' );
						current_state <= WAIT_STATE;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
						current_state <= X_CALC;
					end if;
				when WAIT_STATE =>
					s_axis_divisor_tvalid <= '0';
					s_axis_dividend_tvalid <= '0';
					if ( unsigned(counter) = 10 ) then
						counter <= ( others => '0' );
						current_state <= Y_CALC;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
						current_state <= WAIT_STATE;
					end if;
				when Y_CALC =>
					s_axis_divisor_tvalid <= '1';
					s_axis_dividend_tvalid <= '1';
--					if ( unsigned(counter) = 10 ) then
					if ( div_output_ready = '1' ) then
						counter <= ( others => '0' );
						current_state <= DONE;
					else
						counter <= std_logic_vector( unsigned(counter) + 1 );
						current_state <= Y_CALC;
					end if;
				when DONE =>
					s_axis_divisor_tvalid <= '0';
					s_axis_dividend_tvalid <= '0';
					current_state <= IDLE;
--				when others =>
--					current_state <= IDLE;
			end case;
		end if;
	end process;

	-- output signals
	process( current_state )
	begin
		case current_state is
			when IDLE =>
				FINISHED <= '0';
				mux_select <= '0';
				x_en <= '0';
				y_en <= '0';
			when X_CALC =>
				FINISHED <= '0';
				mux_select <= '0';
				x_en <= '1';
				y_en <= '0';
			when WAIT_STATE =>
				FINISHED <= '0';
				mux_select <= '1';
				x_en <= '0';
				y_en <= '1';
			when Y_CALC =>
				FINISHED <= '0';
				mux_select <= '1';
				x_en <= '0';
				y_en <= '1';
			when DONE =>
				FINISHED <= '1';
				mux_select <= '1';
				x_en <= '0';
				y_en <= '0';
--			when others =>
--				FINISHED <= '0';
--				mux_select <= '0';
--				x_en <= '0';
--				y_en <= '0';
		end case;
	end process;

	-- registers
	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( x_en = '1' ) then
				unshifted_x <= div_out( 32 downto 1 );
			end if;
			if ( y_en = '1' ) then
				unshifted_y <= div_out( 32 downto 1 );
			end if;
		end if;	
	end process;

end architecture Behavioral;
















