library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity ControlMachine is
	generic(
		SELECT_WIDTH : integer;
		SELECT_MAX : integer
	);
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		CENTR_FIN : in std_logic;
		MAGN_FIN : in std_logic;
		NORM_FIN : in std_logic;
		SPR_FIN : in std_logic;
		WIDE_FIFO_FULL : in std_logic;
		CENTR_START : out std_logic;
		MAGN_START : out std_logic;
		NORM_START : out std_logic;
		SPR_START : out std_logic;
		FINISHED : out std_logic;
		WRITE_SIGNAL : out std_logic;
		SELECT_LINE : out std_logic_vector( SELECT_WIDTH downto 1 )
	);
end entity;

architecture behavioral of ControlMachine is

-- type declarations
	type state is ( IDLE, START_CENTROID, WAIT_CENTROID, START_MAGN, WAIT_MAGN,
			START_NORM, WAIT_NORM, START_GROUP1, START_GROUP2, WAIT_GROUP, WRITE_CHECK, WRITE_STATE,
			CHECK_FOR_DONE, DONE );

-- signal declarations
	signal current_state : state := IDLE;
	signal select_value : std_logic_vector( SELECT_WIDTH downto 1 ) := ( others => '0' );
	
begin

-- concurrent signal assignments
	SELECT_LINE <= select_value;

-- process blocks

	-- next state calculation
	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( reset = '1' ) then
				current_state <= IDLE;
			else
				case current_state is
					when IDLE =>
						select_value <= (others => '0');
						if ( START = '1' ) then
							current_state <= START_CENTROID;
						else
							current_state <= IDLE;
						end if;
					when START_CENTROID =>
						current_state <= WAIT_CENTROID;
					when WAIT_CENTROID =>
						if ( CENTR_FIN = '1' ) then
							current_state <= START_MAGN;
						else
							current_state <= WAIT_CENTROID;
						end if;
					when START_MAGN =>
						current_state <= WAIT_MAGN;
					when WAIT_MAGN =>
						if ( MAGN_FIN = '1' ) then
							current_state <= START_NORM;
						else
							current_state <= WAIT_MAGN;
						end if;
					when START_NORM =>
						current_state <= WAIT_NORM;
					when WAIT_NORM =>
						if ( NORM_FIN = '1' ) then
							current_state <= START_GROUP1;
						else
							current_state <= WAIT_NORM;
						end if;
					when START_GROUP1 =>
						current_state <= START_GROUP2;
					when START_GROUP2 =>
						current_state <= WAIT_GROUP;
					when WAIT_GROUP =>
						if ( SPR_FIN = '1' ) then
							current_state <= WRITE_CHECK;
						else
							current_state <= WAIT_GROUP;
						end if;
					when WRITE_CHECK =>
						if ( WIDE_FIFO_FULL = '0' ) then
							current_state <= WRITE_STATE;
						else
							current_state <= WRITE_CHECK;
						end if;
					when WRITE_STATE =>
						current_state <= CHECK_FOR_DONE;
					when CHECK_FOR_DONE =>
						if ( unsigned(select_value) > SELECT_MAX ) then
							current_state <= DONE;
						else
							select_value <= std_logic_vector( unsigned(select_value) + 1 );
							current_state <= START_GROUP1;
						end if;
					when DONE =>
						current_state <= IDLE;
--					when others =>
--						current_state <= IDLE;
				end case;
			end if;
		end if;
	end process;

	-- next state decoding
	process( current_state )
	begin
		case current_state is
			when IDLE =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when START_CENTROID =>
				CENTR_START <= '1';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when WAIT_CENTROID =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when START_MAGN =>
				CENTR_START <= '0';
				MAGN_START <= '1';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when WAIT_MAGN =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when START_NORM =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '1';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when WAIT_NORM =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when START_GROUP1 =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '1';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when START_GROUP2 =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '1';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when WAIT_GROUP =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when WRITE_CHECK =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when WRITE_STATE =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '1';
			when CHECK_FOR_DONE =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '0';
				WRITE_SIGNAL <= '0';
			when DONE =>
				CENTR_START <= '0';
				MAGN_START <= '0';
				NORM_START <= '0';
				SPR_START <= '0';
				FINISHED <= '1';
				WRITE_SIGNAL <= '0';
--			when others =>
--				CENTR_START <= '0';
--				MAGN_START <= '0';
--				NORM_START <= '0';
--				SPR_START <= '0';
--				FINISHED <= '0';
--				WRITE_SIGNAL <= '0';
		end case;
	end process;
	
--	process (clock)
--	begin
--	
--		if (rising_edge(clock)) then
--			if (reset = '1') then
--			
--				sLEDs <= "00000000";
--				
--			else
--		
--				if (CENTR_FIN = '1') then
--					sLEDs(0) <= CENTR_FIN;
--				else
--					sLEDs(0) <= sLEDs(0);
--				end if;
--				
--				if (MAGN_FIN = '1') then
--					sLEDs(1) <= MAGN_FIN;
--				else
--					sLEDs(1) <= sLEDs(1);
--				end if;
--				
--				if (NORM_FIN = '1') then
--					sLEDs(2) <= NORM_FIN;
--				else
--					sLEDs(2) <= sLEDs(2);
--				end if;
--				
--				if (SPR_FIN = '1') then
--					sLEDs(3) <= SPR_FIN;
--				else
--					sLEDs(3) <= sLEDs(3);
--				end if;
--				
--				sLEDs(7 downto 4) <= "1111";
--			
--			end if;
--		end if;
--	
--	end process;

end architecture;


