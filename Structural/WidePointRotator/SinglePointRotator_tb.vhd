library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity SinglePointRotator_tb is
end entity;

architecture tb of SinglePointRotator_tb is

-- component declaration
COMPONENT SinglePointRotatorWrapper is
	port(
		X_0 : in std_logic_vector( 32 downto 1 );
		Y_0 : in std_logic_vector( 32 downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FINISHED : out std_logic;
		X : out std_logic_vector( 32 downto 1 );
		Y : out std_logic_vector( 32 downto 1 )
	);
end COMPONENT;

-- signal declarations
	signal X_0 : std_logic_vector( 32 downto 1 );
	signal Y_0 : std_logic_vector( 32 downto 1 );
	signal X_NORM : std_logic_vector( 32 downto 1 );
	signal Y_NORM : std_logic_vector( 32 downto 1 );
	signal CLOCK : std_logic;
	signal START : std_logic;
	signal RESET : std_logic;
	signal WIDTH : std_logic_vector( 16 downto 1 );
	signal HEIGHT : std_logic_vector( 16 downto 1 );
	signal FINISHED : std_logic;
	signal X : std_logic_vector( 32 downto 1 );
	signal Y : std_logic_vector( 32 downto 1 );

begin

	-- component instantations
	uut : SinglePointRotatorWrapper port map( X_0 => X_0, Y_0 => Y_0, X_NORM => X_NORM,
					   Y_NORM => Y_NORM, CLOCK => CLOCK, START => START,
					   RESET => RESET, FINISHED => FINISHED, X => X, Y => Y,
					   WIDTH => WIDTH, HEIGHT => HEIGHT );

	-- clock proc
	process
	begin
		CLOCK <= '0';
		wait for 10 ns;
		CLOCK <= '1';
		wait for 10 ns;
	end process;

	-- stimulus proc
	process
	begin

		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		HEIGHT <= "00000010" & "00000000";
		WIDTH <= "00000010" & "00000000";
		X_0 <= "00000001" & "00000001" & "00000000" & "00000000";
		Y_0 <= "00000001" & "00000000" & "00000000" & "00000000";
		X_NORM <= "00000000" & "00000000" & "00000000" & "00000000";
		Y_NORM <= "00000000" & "00000001" & "00000000" & "00000000";
		wait for 30 ns;
		START <= '1';
		wait for 30 ns;
		START <= '0';

		wait;

	end process;

end architecture;


