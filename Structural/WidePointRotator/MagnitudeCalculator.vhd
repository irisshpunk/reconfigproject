library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;


entity MagnitudeCalculator is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		MAGNITUDE : out std_logic_vector( 32 downto 1 )
		
		-- debugging
		; LEDs : out std_logic_vector( 8 downto 1 )
	);
end entity;

architecture behavioral of MagnitudeCalculator is

-- types
	type state is ( IDLE, CHECK, SCALE, DONE, SHIFT, COMP, ADD_STATE, SUB_STATE, UPDATE, INCREMENT );
	type LUT is array( integer range <> ) of std_logic_vector( 32 downto 1 );

-- signals
	signal current_state : state := IDLE;
	signal y_copy : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal x_copy : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal x_p_1 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal y_p_1 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal magnitude_signal : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal counter : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp1 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp2 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal corrected_scale_factor : std_logic_vector( 32 downto 1 ) := ( others => '0' );

	constant epsilon : std_logic_vector( 32 downto 1 ) := "00000000" & "00000000" & "00000000" & "00000001";
	constant scale_factor : std_logic_vector( 32 downto 1 ) := "00000000" & "00000000" & "10011011" & "01110101";

	signal scale_factor_LUT : LUT( 15 downto 0 );

	signal A_n : std_logic_vector( 32 downto 1 );

begin
	
	-- the following values were calculated using a script
	scale_factor_LUT( 0 ) <= "00000000000000001011010100000100";
	scale_factor_LUT( 1 ) <= "00000000000000001010000111101000";
	scale_factor_LUT( 2 ) <= "00000000000000001001110100010011";
	scale_factor_LUT( 3 ) <= "00000000000000001001101111011100";
	scale_factor_LUT( 4 ) <= "00000000000000001001101110001110";
	scale_factor_LUT( 5 ) <= "00000000000000001001101101111011";
	scale_factor_LUT( 6 ) <= "00000000000000001001101101110110";
	scale_factor_LUT( 7 ) <= "00000000000000001001101101110101";
	scale_factor_LUT( 8 ) <= "00000000000000001001101101110101";
	scale_factor_LUT( 9 ) <= "00000000000000001001101101110100";
	scale_factor_LUT( 10 ) <= "00000000000000001001101101110100";
	scale_factor_LUT( 11 ) <= "00000000000000001001101101110100";
	scale_factor_LUT( 12 ) <= "00000000000000001001101101110100";
	scale_factor_LUT( 13 ) <= "00000000000000001001101101110100";
	scale_factor_LUT( 14 ) <= "00000000000000001001101101110100";
	scale_factor_LUT( 15 ) <= "00000000000000001001101101110100";

--	A_n <= scale_factor when unsigned(counter) >= 16 else
								

	MAGNITUDE <= magnitude_signal;

--	LEDs <= "11111111" when ( unsigned( counter ) < 16 ) else "01010101";
	LEDs <= counter( 8 downto 1 );


	process( CLOCK ) 
		variable longTemp : std_logic_vector( 64 downto 1 );
		variable shortTemp : std_logic_vector( 32 downto 1 );
	begin
		if ( CLOCK'EVENT AND CLOCK = '1' ) then
			if (reset = '1') then
				current_state <= IDLE;
			else
				case current_state is 
					when IDLE =>
						if ( Y(32) = '1' ) then
							y_copy <= std_logic_vector( unsigned( not Y ) + 1 );
						else
							y_copy <= Y;
						end if;
						if ( X(32) = '1' ) then
							x_copy <= std_logic_vector( unsigned( not X ) + 1 );
						else
							x_copy <= X;
						end if;
						if ( START = '1' ) then
							counter <= ( others => '0' );
							current_state <= CHECK;
						else
							current_state <= IDLE;
						end if;
					when CHECK =>
--						if ( unsigned( counter ) < 16 ) then
--							corrected_scale_factor <= scale_factor_LUT(conv_integer(counter) - 1);
--						else
--							corrected_scale_factor <= scale_factor;
--						end if;
						if ( signed(y_copy) < signed(epsilon) and signed(y_copy) > ( 0 - signed(epsilon) ) ) then
							current_state <= SCALE;
						else
							current_state <= SHIFT;
						end if;
					when SCALE =>
--						if ( unsigned(counter) < 16 ) then
--							longTemp := std_logic_vector( signed(scale_factor_LUT(conv_integer(counter) - 1)) * signed(x_copy) );
--						else
							longTemp := std_logic_vector( signed(scale_factor) * signed(x_copy) );
--						end if;
--						longTemp := std_logic_vector( signed(corrected_scale_factor) * signed(x_copy) );
						longTemp := longTemp( 64 downto 17 ) & "00000000" & "00000000";
						shortTemp := longTemp( 48 downto 17 );
						magnitude_signal <= shortTemp;
						current_state <= DONE;
					when DONE =>
						current_state <= IDLE;
					when SHIFT =>
						temp1 <= std_logic_vector( SHIFT_RIGHT( signed(y_copy), conv_integer( counter ) ) );
						temp2 <= std_logic_vector( SHIFT_RIGHT( signed(x_copy), conv_integer( counter ) ) );
						current_state <= COMP;
					when COMP =>
						if ( signed(y_copy) >= 0 ) then
							current_state <= SUB_STATE;
						else
							current_state <= ADD_STATE;
						end if;
					when ADD_STATE =>
						x_p_1 <= x_copy - temp1;
						y_p_1 <= y_copy + temp2;
						current_state <= UPDATE;
					when SUB_STATE =>
						x_p_1 <= x_copy + temp1;
						y_p_1 <= y_copy - temp2;
						current_state <= UPDATE;
					when UPDATE =>
						x_copy <= x_p_1;
						y_copy <= y_p_1;
						current_state <= INCREMENT;
					when INCREMENT =>
						counter <= std_logic_vector( unsigned(counter) + 1 );
						current_state <= CHECK;
					when others =>
						current_state <= IDLE;
				end case;
			end if;
		end if;
	end process;

	process( current_state )
	begin

		case current_state is
			when DONE =>
				FINISHED <= '1';
			when others =>
				FINISHED <= '0';
		end case;

	end process;


end architecture;

