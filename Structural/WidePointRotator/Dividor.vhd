library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Dividor is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		quotient : out std_logic_vector( 32 downto 1 ) -- X / Y
	);
end entity;

architecture behavioral of Dividor is

-- component declarations

-- type declarations
	type state is ( IDLE, CHECK, EXTRACT, EXTRACT_2, DONE, MATH_1, MATH_2, SHIFT );

-- signal declarations
	signal current_state : state := IDLE;

	signal temp1 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp2 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp3 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp4 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp5 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal temp6 : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal x_copy : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal y_copy : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal z : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal d : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal sign_bit : std_logic;

-- constants
	constant epsilon : std_logic_vector( 32 downto 1 ) := "00000000" & "00000000" & "00000000" & "00000010";
	constant d_init : std_logic_vector( 32 downto 1 ) := "00000000" & "00000001" & "00000000" & "00000000";

begin

	-- concurrent signal assignments
	-- mathy bits
	temp3 <= std_logic_vector( signed(temp1) + signed(temp2) );
	temp6 <= std_logic_vector( signed(temp4) - signed(temp5) );

	-- process blocks
	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			case current_state is
				when IDLE =>
					FINISHED <= '0';
					x_copy <= Y;
					y_copy <= X;
					d <= d_init;
					z <= ( others => '0' );
					if ( START = '1' ) then
						current_state <= CHECK;
					else
						current_state <= IDLE;
					end if;
				when CHECK =>
					sign_bit <= y_copy(32);
					-- ideally, y_copy reaches zero, but if x_copy reaches zero first, there's no more accuracy to be had, so we're done
					if ( ( signed(y_copy) < signed(epsilon) and signed(y_copy) > ( 0 - signed(epsilon) ) ) or ( unsigned(x_copy) = 0 ) ) then
--						current_state <= EXTRACT;
						current_state <= DONE;
					else
						current_state <= MATH_1;
					end if;
				when EXTRACT =>
					temp4 <= (others => '0');
					temp5 <= z;
					current_state <= EXTRACT_2;
				when EXTRACT_2 =>
					quotient <= temp6;
					current_state <= DONE;
				when DONE =>
					FINISHED <= '1';
					quotient <= z;
					current_state <= IDLE;
				when MATH_1 =>
					-- y < 0
					if ( sign_bit = '1' ) then
						-- y_i+1 = y_i + x_i >> i
						temp1 <= y_copy;
						temp2 <= x_copy;
						-- z_i+1 = z_i - d_i >> i
						temp4 <= z;
						temp5 <= d;
					else
						-- y_i+1 = y_i - x_i >> i
						temp4 <= y_copy;
						temp5 <= x_copy;
						-- z_i+1 = z_i + d_i >> i
						temp1 <= z;
						temp2 <= d;
					end if;
					current_state <= MATH_2;
				when MATH_2 =>
					if ( sign_bit = '1' ) then
						y_copy <= temp3;
						z <= temp6;
					else
						y_copy <= temp6;
						z <= temp3;
					end if;
					current_state <= SHIFT;
				when SHIFT =>
					d <= '0' & d( 32 downto 2 );
					x_copy <= '0' & x_copy( 32 downto 2 );
					current_state <= CHECK;
				when others =>
					current_state <= IDLE;
			end case;
		end if;
	end process;

end architecture;


