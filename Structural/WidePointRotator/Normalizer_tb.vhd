library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity Normalizer_tb is
end entity;

architecture behavioral of Normalizer_tb is

COMPONENT Normalizer is
	generic(
		DATA_WIDTH : integer := 32
	);
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		X_0 : in std_logic_vector( DATA_WIDTH downto 1 );
		Y_0 : in std_logic_vector( DATA_WIDTH downto 1 );
		MAGN : in std_logic_vector( DATA_WIDTH downto 1 );
		START : in std_logic;
		FINISHED : out std_logic;
		X_NORM : out std_logic_vector( DATA_WIDTH downto 1 );
		Y_NORM : out std_logic_vector( DATA_WIDTH downto 1 )
	);
end COMPONENT;

	signal CLOCK : std_logic := '0';
	signal RESET : std_logic := '0';
	signal X_0 : std_logic_vector( 32 downto 1 );
	signal Y_0 : std_logic_vector( 32 downto 1 );
	signal MAGN : std_logic_vector( 32 downto 1 );
	signal START : std_logic := '0';
	signal FINISHED : std_logic;
	signal X_NORM : std_logic_vector( 32 downto 1 );
	signal Y_NORM : std_logic_vector( 32 downto 1 );

begin

	uut : Normalizer generic map( DATA_WIDTH => 32 ) port map( CLOCK => CLOCK, RESET => RESET,
								   X_0 => X_0, Y_0 => Y_0, MAGN => MAGN,
								   START => START, FINISHED => FINISHED,
								   X_NORM => X_NORM, Y_NORM => Y_NORM );


	process
	begin
		CLOCK <= '0';
		wait for 10 ns;
		CLOCK <= '1';
		wait for 10 ns;
	end process;


	process
	begin

		-- reset
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';

		-- stimulus

		X_0 <= "00000000" & "00000001" & "00000000" & "00000000";
		Y_0 <= "00000000" & "00000001" & "00000000" & "00000000";
		MAGN <= "00000000" & "00000001" & "01101010" & "00001010";
		wait for 20 ns;
		START <= '1';
		wait for 40 ns;
		START <= '0';

		wait;
	end process;

end architecture behavioral;

