library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity SinglePointRotator is
	port(
		X_0 : in std_logic_vector( 32 downto 1 );
		Y_0 : in std_logic_vector( 32 downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		FINISHED : out std_logic;
		X : out std_logic_vector( 16 downto 1 );
		Y : out std_logic_vector( 16 downto 1 )
	);
end entity;

architecture Behavioral of SinglePointRotator is

-- component declarations
COMPONENT DataReg is
	generic(
		width : integer := 8
	);
	port(
		D : in std_logic_vector ( width downto 1 );
		Q : out std_logic_vector( width downto 1 );
		ENABLE : in std_logic;
		CLOCK : in std_logic;
		RESET : in std_logic
	);
end COMPONENT;


-- type declarations
	type state is ( IDLE, PRE_WAIT_1, EX_EX, WAIT_1, WAIT_1_2, PRE_WAIT_2, WY_WY, WAIT_2, WAIT_2_2, 
							PRE_WAIT_3, EX_WY, WAIT_3, WAIT_3_2, PRE_WAIT_4, WY_EX, WAIT_4, WAIT_4_2, READY );

-- signal declarations
	signal select_line : std_logic_vector( 2 downto 1 ) := "00";
	signal mult_in_1 : std_logic_vector( 32 downto 1 );
	signal mult_in_2 : std_logic_vector( 32 downto 1 );
	signal mult_in_1_prime : std_logic_vector( 16 downto 1 );
	signal mult_in_2_prime : std_logic_vector( 17 downto 1 );
	signal mult_in_1_prime_pipe : std_logic_vector( 16 downto 1 );
	signal mult_in_2_prime_pipe : std_logic_vector( 17 downto 1 );
	signal mult_out : std_logic_vector( 64 downto 1 );
	signal culled_mult_out : std_logic_vector( 32 downto 1 );
	signal subtr_in_1 : std_logic_vector( 16 downto 1 );
	signal subtr_in_2 : std_logic_vector( 16 downto 1 );
	signal add_in_1 : std_logic_vector( 16 downto 1 );
	signal add_in_2 : std_logic_vector( 16 downto 1 );

	signal d_en_1 : std_logic;
	signal d_en_2 : std_logic;
	signal d_en_3 : std_logic;
	signal d_en_4 : std_logic;

	signal current_state : state := IDLE;
	
	signal pipelined : std_logic_vector( 16 downto 1 );
	
begin

	-- component instantiations
	ex_ex_reg : DataReg generic map( width => 16 ) port map( D => pipelined, Q => subtr_in_1, ENABLE => d_en_1, CLOCK => CLOCK,
								 RESET => RESET );
	wy_wy_reg : DataReg generic map( width => 16 ) port map( D => pipelined, Q => subtr_in_2, ENABLE => d_en_2, CLOCK => CLOCK,
								 RESET => RESET );
	ex_wy_reg : DataReg generic map( width => 16 ) port map( D => pipelined, Q => add_in_1, ENABLE => d_en_3, CLOCK => CLOCK,
								 RESET => RESET ); 
	wy_ex_reg : DataReg generic map( width => 16 ) port map( D => pipelined, Q => add_in_2, ENABLE => d_en_4, CLOCK => CLOCK,
								 RESET => RESET );

	pipe : DataReg generic map( width => 16 ) port map( D => culled_mult_out( 32 downto 17 ), Q => pipelined, ENABLE => '1', CLOCK => CLOCK,
								  RESET => RESET );

	pipe_2 : DataReg generic map( width => 16 ) port map( D => mult_in_1_prime, Q => mult_in_1_prime_pipe, ENABLE => '1', CLOCK => CLOCK,
																			RESET => RESET );
																			
	pipe_3 : DataReg generic map( width => 17 ) port map( D => mult_in_2_prime, Q => mult_in_2_prime_pipe, ENABLE => '1', CLOCK => CLOCK,
																			RESET => RESET );

	-- concurrent signals
	-- d1 <= x * x_norm
	d_en_1 <= ( not select_line(1) ) and ( not select_line(2) );
	-- d2 <= y * y_norm
	d_en_2 <= ( select_line(1) ) and ( select_line(2) );
	-- d3 <= x_norm * y
	d_en_3 <= ( not select_line(1) ) and ( select_line(2) );
	-- d4 <= x * y_norm
	d_en_4 <= ( select_line(1) ) and ( not select_line(2) );

--	with select_line select
--		mult_in_1 <= X_0 when "00",
--			     Y_0 when "10",
--			     X_NORM when "01",
--			     X_0 when "11",
--			     ( others => '0') when others;

	with select_line(2) select
		mult_in_1_prime <= X_0( 32 downto 17 ) when '0',
								 Y_0( 32 downto 17 ) when '1',
								( others => '0' ) when others;

--	with select_line select
--		mult_in_2 <= X_NORM when "00",
--			     Y_NORM when "10",
--			     Y_0 when "01",
--			     Y_NORM when "11",
--			     ( others => '0' ) when others;

	with select_line(1) select
		mult_in_2_prime <= X_NORM( 17 downto 1 ) when '0',
								 Y_NORM( 17 downto 1 ) when '1',
								( others => '0' ) when others;
								
	mult_in_2 <= "00000000" & "0000000" & mult_in_2_prime_pipe;
	mult_in_1 <= mult_in_1_prime_pipe & "00000000" & "00000000";

	X <= std_logic_vector( signed(subtr_in_1) - signed(subtr_in_2) );
	Y <= std_logic_vector( signed(add_in_1) + signed(add_in_2) );
	mult_out <= std_logic_vector( signed(mult_in_1) * signed(mult_in_2) );
	culled_mult_out <= mult_out( 48 downto 17 );

	-- process blocks
	process( CLOCK )
	begin

		if ( rising_edge(CLOCK) ) then
			if ( RESET = '1' ) then
				current_state <= IDLE;
			else
			case current_state is
				when IDLE =>
					FINISHED <= '0';
					select_line <= "00";
					if ( START = '1' ) then
						current_state <= PRE_WAIT_1;
					else
						current_state <= IDLE;
					end if;
				when PRE_WAIT_1 =>
					FINISHED <= '0';
					select_line <= "00";
					current_state <= EX_EX;
				when EX_EX =>
					FINISHED <= '0';
					select_line <= "00";
					current_state <= WAIT_1;
				when WAIT_1 =>
					FINISHED <= '0';
					select_line <= "00";
					current_state <= WAIT_1_2;
				when WAIT_1_2 =>
					FINISHED <= '0';
					select_line <= "00";
					current_state <= PRE_WAIT_2;
				when PRE_WAIT_2 =>
					FINISHED <= '0';
					select_line <= "11";
					current_state <= WY_WY;
				when WY_WY =>
					FINISHED <= '0';
					select_line <= "11";
					current_state <= WAIT_2;
				when WAIT_2 =>
					FINISHED <= '0';
					select_line <= "11";
					current_state <= WAIT_2_2;
				when WAIT_2_2 =>
					FINISHED <= '0';
					select_line <= "11";
					current_state <= PRE_WAIT_3;
				when PRE_WAIT_3 =>
					FINISHED <= '0';
					select_line <= "10";
					current_state <= EX_WY;
				when EX_WY =>
					FINISHED <= '0';
					select_line <= "10";
					current_state <= WAIT_3;
				when WAIT_3 =>
					FINISHED <= '0';
					select_line <= "10";
					current_state <= WAIT_3_2;
				when WAIT_3_2 =>
					FINISHED <= '0';
					select_line <= "10";
					current_state <= PRE_WAIT_4;
				when PRE_WAIT_4 =>
					FINISHED <= '0';
					select_line <= "01";
					current_state <= WY_EX;
				when WY_EX =>
					FINISHED <= '0';
					select_line <= "01";
					current_state <= WAIT_4;
				when WAIT_4 =>
					FINISHED <= '0';
					select_line <= "01";
					current_state <= WAIT_4_2;
				when WAIT_4_2 =>
					FINISHED <= '0';
					select_line <= "01";
					current_state <= READY;
				when READY =>
					FINISHED <= '1';
					select_line <= "01";
					current_state <= IDLE;
				when others =>
					FINISHED <= '0';
					select_line <= "00";
					current_state <= IDLE;
			end case;
			end if;
		end if;

	end process;



end architecture Behavioral;

