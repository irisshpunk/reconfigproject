library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity SinglePointRotatorWrapper is
	port(
		X_0 : in std_logic_vector( 16 downto 1 );
		Y_0 : in std_logic_vector( 16 downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FINISHED : out std_logic;
		X : out std_logic_vector( 16 downto 1 );
		Y : out std_logic_vector( 16 downto 1 )
	);
end entity;

architecture Structural of SinglePointRotatorWrapper is

COMPONENT DataReg is
	generic(
		width : integer := 8
	);
	port(
		D : in std_logic_vector ( width downto 1 );
		Q : out std_logic_vector( width downto 1 );
		ENABLE : in std_logic;
		CLOCK : in std_logic;
		RESET : in std_logic
	);
end COMPONENT;

COMPONENT SinglePointRotator is
	port(
		X_0 : in std_logic_vector( 32 downto 1 );
		Y_0 : in std_logic_vector( 32 downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		FINISHED : out std_logic;
		X : out std_logic_vector( 16 downto 1 );
		Y : out std_logic_vector( 16 downto 1 )
	);
end COMPONENT;

-- signal declarations
	signal shifted_x_0 : std_logic_vector( 32 downto 1 );
	signal shifted_y_0 : std_logic_vector( 32 downto 1 );
	
	signal shifted_x_0_prime : std_logic_vector( 16 downto 1 );
	signal shifted_y_0_prime : std_logic_vector( 16 downto 1 );

	signal unshifted_x : std_logic_vector( 16 downto 1 );
	signal unshifted_y : std_logic_vector( 16 downto 1 );

	signal half_height : std_logic_vector( 16 downto 1 );
	signal half_width : std_logic_vector( 16 downto 1 );

	signal checked_X : std_logic_vector( 16 downto 1 );
	signal checked_Y : std_logic_vector( 16 downto 1 );

	signal start_vec : std_logic_vector( 1 downto 1 );
	signal start_pipe : std_logic_vector( 1 downto 1 );
	signal x_norm_pipe : std_logic_vector( 32 downto 1 );
	signal y_norm_pipe : std_logic_vector( 32 downto 1 );
	signal x_0_pipe : std_logic_vector( 32 downto 1 );
	signal y_0_pipe : std_logic_vector( 32 downto 1 );

begin

-- component instantiations
	core : SinglePointRotator port map ( X_0 => x_0_pipe, Y_0 => y_0_pipe,
					     X_NORM => x_norm_pipe, Y_NORM => y_norm_pipe, CLOCK => CLOCK,
					     START => start_pipe(1), 
						  RESET => RESET, FINISHED => FINISHED,
					     X => unshifted_x, Y => unshifted_y );

	start_reg : DataReg generic map( width => 1 ) port map( D => start_vec, Q => start_pipe, ENABLE => '1', CLOCK => CLOCK,
								 RESET => RESET );

	x_norm_reg : DataReg generic map( width => 32 ) port map( D => X_NORM, Q => x_norm_pipe, ENABLE => '1', CLOCK => CLOCK,
								 RESET => RESET );

	y_norm_reg : DataReg generic map( width => 32 ) port map( D => Y_NORM, Q => y_norm_pipe, ENABLE => '1', CLOCK => CLOCK,
								 RESET => RESET );

	x_0_reg : DataReg generic map( width => 32 ) port map( D => shifted_x_0, Q => x_0_pipe, ENABLE => '1', CLOCK => CLOCK,
								 RESET => RESET );
								 
	y_0_reg : DataReg generic map( width => 32 ) port map( D => shifted_y_0, Q => y_0_pipe, ENABLE => '1', CLOCK => CLOCK,
								 RESET => RESET );

-- concurrent signal assignments
	half_height <= '0' & HEIGHT( 16 downto 2 );-- & "00000000" & "00000000";
	half_width <= '0' & WIDTH( 16 downto 2 );-- & "00000000" & "00000000";
	shifted_x_0_prime <= std_logic_vector( signed( X_0 ) - signed( half_width ) );
	shifted_y_0_prime <= std_logic_vector( signed( Y_0 ) - signed( half_height ) );
	shifted_x_0 <= shifted_x_0_prime & "00000000" & "00000000";
	shifted_y_0 <= shifted_y_0_prime & "00000000" & "00000000";
	checked_X <= std_logic_vector( signed( unshifted_x ) + signed( half_width ) );
	checked_Y <= std_logic_vector( signed( unshifted_y ) + signed( half_height ) );
	start_vec(1) <= START;

-- process blocks
	process( checked_X, width )
	begin
		if ( signed(checked_X) >= signed(WIDTH) ) then
			X <= std_logic_vector( signed(WIDTH) - 1 );
		else
			if ( signed(checked_X) < 0 ) then
				X <= ( others => '0' );
			else
				X <= checked_X;
			end if;
		end if;
	end process;

	process( checked_Y, height )
	begin
		if ( signed(checked_Y) >= signed(HEIGHT) ) then
			Y <= std_logic_vector( signed(HEIGHT) - 1 );
		else
			if ( signed(checked_Y) < 0 ) then
				Y <= ( others => '0' );
			else
				Y <= checked_Y;
			end if;
		end if;
	end process;

end architecture Structural;

