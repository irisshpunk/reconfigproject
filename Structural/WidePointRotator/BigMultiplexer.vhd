library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.TypePackage.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity BigMultiplexer is
	generic(
		SELECT_WIDTH : integer := 3;
		DATA_WIDTH : integer := 16
	);
	port(
		SELECT_LINE : in std_logic_vector( SELECT_WIDTH downto 1 );
		DATA_LINE : in std_logic_vector( (DATA_WIDTH * ((2**SELECT_WIDTH)))-1 downto 0 );
		DATA_OUT : out std_logic_vector( DATA_WIDTH downto 1 )
	);
end entity;

architecture behavioral of BigMultiplexer is
begin

	-- concurrent statements

	-- process blocks
	process( SELECT_LINE, DATA_LINE )
		variable index : integer;
	begin
		index := to_integer(unsigned(SELECT_LINE));
		DATA_OUT <= DATA_LINE( ((index+1) * DATA_WIDTH) - 1 downto ((index) * DATA_WIDTH ) );
	end process;

end architecture behavioral;

