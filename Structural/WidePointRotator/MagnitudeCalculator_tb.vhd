library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_SIGNED.ALL;

entity MagnitudeCalculator_tb is
end entity;

architecture behavioral of MagnitudeCalculator_tb is

COMPONENT MagnitudeCalculator is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		MAGNITUDE : out std_logic_vector( 32 downto 1 )
	);
end COMPONENT;

	signal CLOCK  : std_logic := '0';
	signal START : std_logic := '0';
	signal RESET : std_logic := '0';
	signal FINISHED : std_logic;
	signal X : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal Y : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal MAGNITUDE : std_logic_vector( 32 downto 1 );


	signal magnitudeTimer : integer := 0;

begin

	uut : MagnitudeCalculator port map( CLOCK => CLOCK, RESET => RESET, START => START, FINISHED => FINISHED, X => X, Y => Y, MAGNITUDE => MAGNITUDE );


	process
	begin
		CLOCK <= '0';
		wait for 10 ns;
		CLOCK <= '1';
		wait for 10 ns;
		magnitudeTimer <= magnitudeTimer + 1;
	end process;

	process
	begin
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		X <= "00000000" & "00000000" & "00000000" & "00000000";
		Y <= "00000000" & "00000001" & "00000000" & "00000000";
		wait for 30 ns;
		START <= '1';
		wait for 30 ns;
		START <= '0';
		wait for 20 ns;
		wait until FINISHED = '1';
		wait for 50 ns;
		X <= "00000000" & "00000001" & "00000000" & "00000000";
		Y <= "00000000" & "00000001" & "00000000" & "00000000";
		wait for 20 ns;
		START <= '1';
		wait for 30 ns;
		START <= '0';
		wait for 20 ns;
		wait until FINISHED = '1';
		X <= "00000000" & "11000000" & "00000000" & "00000000";
		Y <= "00000000" & "00000011" & "00000000" & "00000000";
		wait for 20 ns;
		START <= '1';
		wait for 30 ns;
		START <= '0';
		wait for 20 ns;
		wait until FINISHED = '1';
		wait for 50 ns;
		X <= "00000000" & "00110000" & "00101011" & "11000000";
		Y <= "00001100" & "00011100" & "11100001" & "00000000"; 
		wait for 30 ns;
		START <= '1';
		wait for 30 ns;
		START <= '0';
		wait for 30 ns;
		wait until FINISHED = '1';
		wait for 50 ns;
		X <= "11100000" & "00110000" & "00101011" & "11000000";
		Y <= "00001100" & "00011100" & "11100001" & "00000000"; 
		wait for 30 ns;
		START <= '1';
		wait for 30 ns;
		START <= '0';
		wait for 30 ns;
		wait until FINISHED = '1';

		wait;
	end process;


end architecture;
