library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.TypePackage.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity SinglePointRotatorBank is
	generic(
		simultaneous_inst : integer := 8;
		bits_per_axis : integer := 16
	);
	port(
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		DATA_IN : in std_logic_vector( 2 * simultaneous_inst * bits_per_axis downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FINISHED : out std_logic;
		DATA_OUT : out std_logic_vector( 2 * simultaneous_inst * bits_per_axis downto 1 )
	);
end entity;


architecture structural of SinglePointRotatorBank is

-- component declarations
COMPONENT SinglePointRotatorWrapper is
	port(
		X_0 : in std_logic_vector( 16 downto 1 );
		Y_0 : in std_logic_vector( 16 downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FINISHED : out std_logic;
		X : out std_logic_vector( 16 downto 1 );
		Y : out std_logic_vector( 16 downto 1 )
	);
end COMPONENT;

-- type declarations
	type long_signal is array( natural range<> ) of std_logic_vector( 32 downto 1 );

-- signal declarations
	signal FINISHED_OUT : std_logic_vector( simultaneous_inst downto 1 );
	signal extended_x_0 : long_signal( simultaneous_inst downto 1 );
	signal extended_y_0 : long_signal( simultaneous_inst downto 1 );
	
	type led_array is array( natural range<> ) of std_logic_vector(7 downto 0);

begin

-- component instantiations
	SPR_GEN : 
	for i in 1 to simultaneous_inst generate
		SPR_BLOCK : SinglePointRotatorWrapper port map( X_0 => DATA_IN( ( bits_per_axis * ( 2 * i - 1 ) ) downto ( bits_per_axis * ( 2 * i - 2 ) ) + 1 ),
								Y_0 => DATA_IN( ( bits_per_axis * ( 2 * i ) ) downto ( bits_per_axis * ( 2 * i - 1 ) ) + 1 ),
								X_NORM => X_NORM, Y_NORM => Y_NORM, CLOCK => CLOCK, START => START, RESET => RESET,
								WIDTH => WIDTH, HEIGHT => HEIGHT, FINISHED => FINISHED_OUT( i ),
								X => DATA_OUT( ( bits_per_axis * ( 2 * i - 1 ) ) downto ( bits_per_axis * ( 2 * i - 2 ) ) + 1 ),
								Y => DATA_OUT( ( bits_per_axis * ( 2 * i ) ) downto ( bits_per_axis * ( 2 * i - 1 ) ) + 1 ) );
	end generate SPR_GEN;

-- concurrent signal assignments
	FINISHED <= FINISHED_OUT( 1 );

-- process blocks
--	process( DATA_IN )
--	begin
--		for i in 1 to simultaneous_inst loop
--			extended_x_0( i ) <= DATA_IN( ( bits_per_axis * ( 2 * i - 1 ) ) downto ( bits_per_axis * ( 2 * i - 2 ) ) + 1 ) & "00000000" & "00000000";
--			extended_y_0( i ) <= DATA_IN( ( bits_per_axis * ( 2 * i ) ) downto ( bits_per_axis * ( 2 * i - 1 ) ) + 1 ) & "00000000" & "00000000";
--		end loop;
--	end process;

end architecture;



