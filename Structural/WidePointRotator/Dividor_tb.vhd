library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity Dividor_tb is
end entity;

architecture behavioral of Dividor_tb is

COMPONENT Dividor is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		quotient : out std_logic_vector( 32 downto 1 ) -- X / Y
	);
end COMPONENT;

	signal CLOCK  : std_logic := '0';
	signal START : std_logic := '0';
	signal RESET : std_logic := '0';
	signal FINISHED : std_logic;
	signal X : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal Y : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal quotient : std_logic_vector( 32 downto 1 );

	signal divider_timer : std_logic_vector( 32 downto 1 ) := ( others => '0' );

begin

	uut : Dividor port map( CLOCK => CLOCK, RESET => RESET, START => START, FINISHED => FINISHED,
				X => X, Y => Y, quotient => quotient );

	process
	begin
		CLOCK <= '0';
		wait for 5 ns;
		CLOCK <= '1';
		wait for 5 ns;
		divider_timer <= std_logic_vector( unsigned( divider_timer ) + 1 );
	end process;


	process
	begin
		RESET <= '1';
		wait for 100 ns;
		
		RESET <= '0';
		X <= "00000000" & "00000001" & "00000000" & "00000000";
		Y <= "00000000" & "11111111" & "00000000" & "00000000";
		START <= '1';
		wait for 20 ns;
		START <= '0';
		wait until FINISHED = '1';
		wait for 30 ns;
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		wait for 30 ns;
		X <= "00000000" & "00000000" & "00000110" & "00000000";
		Y <= "00000000" & "00000001" & "00000000" & "00000000";
		START <= '1';
		wait for 20 ns;
		START <= '0';
		wait until FINISHED = '1';
		wait for 30 ns;
		X <= "00000000" & "00000000" & "11000000" & "00000000";
		Y <= "00000000" & "01100000" & "00000000" & "00000000";
		START <= '1';
		wait for 30 ns;
		START <= '0';
		wait until FINISHED = '1';
		wait for 30 ns;

		wait;
	end process;

end architecture;
