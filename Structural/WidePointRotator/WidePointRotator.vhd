library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use work.TypePackage.all;

entity WidePointRotator is
	generic(
		SIMULTANEOUS_INSTANCES_HIGH : integer;
		SELECT_WIDTH_HIGH : integer;
		NUMBER_OF_ITERATIONS : integer;
		BITS_PER_AXIS_HIGH : integer := 16
	);
	port(
		LEDs : out std_logic_vector(7 downto 0);
		m_10 : in std_logic_vector( 64 downto 1 );
		m_01 : in std_logic_vector( 64 downto 1 );
		m_00 : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FIFO_FULL : in std_logic;
		FINISHED : out std_logic;
		WRITE_SIGNAL : out std_logic;
		DATA_OUT : out std_logic_vector( 2 * SIMULTANEOUS_INSTANCES_HIGH * BITS_PER_AXIS_HIGH downto 1 )
	);
end entity;

architecture Structural of WidePointRotator is

-- component declarations
COMPONENT ConstantPool is
	generic(
		bits_per_axis : integer := 16;
		number_of_points : integer := 512
	);
	port(
		DATA_OUT : out std_logic_vector( 2 * bits_per_axis * number_of_points downto 1 )
	);
end COMPONENT;

COMPONENT BigMultiplexer is
	generic(
		SELECT_WIDTH : integer := 3;
		DATA_WIDTH : integer := 16
	);
	port(
		SELECT_LINE : in std_logic_vector( SELECT_WIDTH downto 1 );
		DATA_LINE : in std_logic_vector( (DATA_WIDTH * ((2**SELECT_WIDTH)))-1 downto 0 );
		DATA_OUT : out std_logic_vector( DATA_WIDTH downto 1 )
	);
end COMPONENT;

COMPONENT SinglePointRotatorBank is
	generic(
		simultaneous_inst : integer := 8;
		bits_per_axis : integer := 16
	);
	port(
		CLOCK : in std_logic;
		START : in std_logic;
		RESET : in std_logic;
		DATA_IN : in std_logic_vector( 2 * simultaneous_inst * bits_per_axis downto 1 );
		X_NORM : in std_logic_vector( 32 downto 1 );
		Y_NORM : in std_logic_vector( 32 downto 1 );
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FINISHED : out std_logic;
		DATA_OUT : out std_logic_vector( 2 * simultaneous_inst * bits_per_axis downto 1 )
	);
end COMPONENT;

COMPONENT ControlMachine is
	generic(
		SELECT_WIDTH : integer;
		SELECT_MAX : integer
	);
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		CENTR_FIN : in std_logic;
		MAGN_FIN : in std_logic;
		NORM_FIN : in std_logic;
		SPR_FIN : in std_logic;
		WIDE_FIFO_FULL : in std_logic;
		CENTR_START : out std_logic;
		MAGN_START : out std_logic;
		NORM_START : out std_logic;
		SPR_START : out std_logic;
		FINISHED : out std_logic;
		WRITE_SIGNAL : out std_logic;
		SELECT_LINE : out std_logic_vector( SELECT_WIDTH downto 1 )
	);
end COMPONENT;

COMPONENT CentroidCalculator is
	port(
		m_10 : in std_logic_vector( 64 downto 1 );
		m_01 : in std_logic_vector( 64 downto 1 );
		m_00 : in std_logic_vector( 32 downto 1 );
		width : in std_logic_vector( 16 downto 1 );
		height : in std_logic_vector( 16 downto 1 );
		START : in std_logic;
		CLOCK : in std_logic;
		FINISHED : out std_logic;
		x_0 : out std_logic_vector( 32 downto 1 );
		y_0 : out std_logic_vector( 32 downto 1 )
	);
end COMPONENT;

COMPONENT MagnitudeCalculator is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		MAGNITUDE : out std_logic_vector( 32 downto 1 )
		
		-- debugging
		; LEDs : out std_logic_vector( 8 downto 1 )
	);
end COMPONENT;

COMPONENT Normalizer is
	generic(
		DATA_WIDTH : integer := 32
	);
	port(
		LEDs : out std_logic_vector(7 downto 0);
		CLOCK : in std_logic;
		RESET : in std_logic;
		X_0 : in std_logic_vector( DATA_WIDTH downto 1 );
		Y_0 : in std_logic_vector( DATA_WIDTH downto 1 );
		MAGN : in std_logic_vector( DATA_WIDTH downto 1 );
		START : in std_logic;
		FINISHED : out std_logic;
		X_NORM : out std_logic_vector( DATA_WIDTH downto 1 );
		Y_NORM : out std_logic_vector( DATA_WIDTH downto 1 )
	);
end COMPONENT;


-- signal declarations
	-- connect constant pool to multiplexer
	signal constant_pool_out : std_logic_vector( 4 * 256 * BITS_PER_AXIS_HIGH downto 1 );
	--signal mux_in : wide_mux_input( 2**SELECT_WIDTH_HIGH downto 1, 2 * BITS_PER_AXIS_HIGH downto 1 );
	-- connect control machine to multiplexer
	signal select_line : std_logic_vector( SELECT_WIDTH_HIGH downto 1 );

	-- connect multiplexer to SPR bank
	signal mux_out : std_logic_vector( 2 * SIMULTANEOUS_INSTANCES_HIGH * BITS_PER_AXIS_HIGH downto 1  );

	-- connect control machine to SPR bank
	signal SPR_START : std_logic := '0';
	signal SPR_FIN : std_logic;

	-- connect control machine to centr/magn/norm pipeline
	signal CENTR_START : std_logic := '0';
	signal CENTR_FIN : std_logic;
	signal MAGN_START : std_logic := '0';
	signal MAGN_FIN : std_logic;
	signal NORM_START : std_logic := '0';
	signal NORM_FIN : std_logic;

	-- connect centr/magn/norm pipeline to each other
	signal x_0 : std_logic_vector( 32 downto 1 );
	signal y_0 : std_logic_vector( 32 downto 1 );
	signal corrected_x_0 : std_logic_vector( 32 downto 1 );
	signal corrected_y_0 : std_logic_vector( 32 downto 1 );
	signal magnitude : std_logic_vector( 32 downto 1 );
	signal x_norm : std_logic_vector( 32 downto 1 );
	signal y_norm : std_logic_vector( 32 downto 1 );


	-- debugging signals
--	signal axis_1 : std_logic_vector( 16 downto 1 );
--	signal axis_2 : std_logic_vector( 16 downto 1 );

	-- connect pipeline to SPR bank
	signal y_norm_neg : std_logic_vector( 32 downto 1 );

begin

-- component instantiations
	c_pool : ConstantPool generic map( bits_per_axis => BITS_PER_AXIS_HIGH, NUMBER_OF_POINTS => 512 )
			      port map( DATA_OUT => constant_pool_out );

	mux : BigMultiplexer generic map( SELECT_WIDTH => SELECT_WIDTH_HIGH, DATA_WIDTH => SIMULTANEOUS_INSTANCES_HIGH * 2 * BITS_PER_AXIS_HIGH )
			     port map( SELECT_LINE => select_line, DATA_LINE => constant_pool_out, DATA_OUT => mux_out );

	spr_bank : SinglePointRotatorBank generic map( simultaneous_inst => SIMULTANEOUS_INSTANCES_HIGH, bits_per_axis => BITS_PER_AXIS_HIGH )
					  port map( CLOCK => CLOCK, RESET => RESET, START => SPR_START, DATA_IN => mux_out, X_NORM => x_norm,
						    Y_NORM => y_norm_neg, --Y_NORM => y_norm, 
						    WIDTH => WIDTH, HEIGHT => HEIGHT, FINISHED => spr_fin, DATA_OUT => DATA_OUT );

	controller : ControlMachine generic map( SELECT_WIDTH => SELECT_WIDTH_HIGH, SELECT_MAX => NUMBER_OF_ITERATIONS )
				    port map( CLOCK => CLOCK, RESET => RESET, START => START, CENTR_FIN => CENTR_FIN, MAGN_FIN => MAGN_FIN,
					      NORM_FIN => NORM_FIN, SPR_FIN => SPR_FIN, WIDE_FIFO_FULL => FIFO_FULL, CENTR_START => CENTR_START,
					      MAGN_START => MAGN_START, NORM_START => NORM_START, SPR_START => SPR_START, FINISHED => FINISHED,
					      WRITE_SIGNAL => WRITE_SIGNAL, SELECT_LINE => select_line );

	centr_calc : CentroidCalculator port map( m_10 => m_10, m_01 => m_01, m_00 => m_00, width => WIDTH, height => HEIGHT, START => CENTR_START,
						  CLOCK => CLOCK, FINISHED => CENTR_FIN, x_0 => x_0, y_0 => y_0 );

	magn_calc : MagnitudeCalculator port map( CLOCK => CLOCK, RESET => RESET, START => MAGN_START, FINISHED => MAGN_FIN, X => corrected_x_0, --X => x_0,
						  Y => corrected_y_0, --Y => y_0, 
						  MAGNITUDE => magnitude,
						  LEDs => LEDs );

	normalizer_unit : Normalizer generic map( DATA_WIDTH => 2 * BITS_PER_AXIS_HIGH )
				     port map( --LEDs => LEDs, 
							CLOCK => CLOCK, RESET => RESET, X_0 => corrected_x_0, --X_0 => x_0, 
					       Y_0 => corrected_y_0, --Y_0 => y_0, 
					       MAGN => magnitude, START => NORM_START,
					       FINISHED => NORM_FIN, X_NORM => x_norm, Y_NORM => y_norm );

-- concurrent signal assignments
	corrected_x_0 <= x_0( 16 downto 1 ) & "00000000" & "00000000";
	corrected_y_0 <= y_0( 16 downto 1 ) & "00000000" & "00000000";
	--y_norm_neg <= std_logic_vector( 0 - signed( y_norm ) );
	y_norm_neg <= y_norm;
	-- debugging
--	axis_1 <= mux_out( 16 downto 1 );
--	axis_2 <= mux_out( 32 downto 17 );

-- process blocks

end architecture;


