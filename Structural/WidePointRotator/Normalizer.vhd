library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity Normalizer is
	generic(
		DATA_WIDTH : integer := 32
	);
	port(
		LEDs : out std_logic_vector(7 downto 0);
		CLOCK : in std_logic;
		RESET : in std_logic;
		X_0 : in std_logic_vector( DATA_WIDTH downto 1 );
		Y_0 : in std_logic_vector( DATA_WIDTH downto 1 );
		MAGN : in std_logic_vector( DATA_WIDTH downto 1 );
		START : in std_logic;
		FINISHED : out std_logic;
		X_NORM : out std_logic_vector( DATA_WIDTH downto 1 );
		Y_NORM : out std_logic_vector( DATA_WIDTH downto 1 )
	);
end entity;

architecture structural of Normalizer is

-- component declarations
COMPONENT DataReg is
	generic(
		width : integer := 8
	);
	port(
		D : in std_logic_vector ( width downto 1 );
		Q : out std_logic_vector( width downto 1 );
		ENABLE : in std_logic;
		CLOCK : in std_logic;
		RESET : in std_logic
	);
end COMPONENT;

COMPONENT Dividor is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		quotient : out std_logic_vector( 32 downto 1 ) -- X / Y
	);
end COMPONENT;

-- type declarations
	-- state machine stuff
	type state is ( IDLE, LOAD_X_0, WAIT_X_0, WASTE_1, LOAD_Y_0, PROC_Y_0, WAIT_Y_0, WASTE_2, SPIKE );

-- signal declarations
	-- CORDIC control signals
	signal div_start : std_logic := '0';
	signal div_fin : std_logic;

	-- CORDIC data signals
	signal x_in : std_logic_vector( DATA_WIDTH downto 1 );
	signal q_out : std_logic_vector( DATA_WIDTH downto 1 );

	-- input control signals
	signal select_signal : std_logic := '0';
	signal inverted_select_signal : std_logic := '1';

	-- state machine controls
	signal current_state : state := IDLE;
	
	signal sLEDs : std_logic_vector(7 downto 0);
	signal x : std_logic_vector( DATA_WIDTH downto 1 );
	signal y : std_logic_vector( DATA_WIDTH downto 1 );

begin

	LEDs <= sLEDs;
	X_NORM <= x;
	Y_NORM <= y;
	sLEDs <= x(17 downto 10);

	-- component instantiations
	x_out : DataReg generic map( width => 32 ) port map( D => q_out, Q => x, ENABLE => inverted_select_signal, CLOCK => CLOCK, RESET => RESET );
	y_out : DataReg generic map( width => 32 ) port map( D => q_out, Q => y, ENABLE => select_signal, CLOCK => CLOCK, RESET => RESET );
	math_center : Dividor port map( CLOCK => CLOCK, RESET => RESET, START => div_start, FINISHED => div_fin, X => x_in, Y => MAGN,
					quotient => q_out );

	-- concurrent signal assignment statements
	with select_signal select
		x_in <= X_0 when '0',
			Y_0 when '1',
			( others => '0' ) when others;


	-- process blocks

	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( RESET = '1' ) then
				current_state <= IDLE;
			else
				case current_state is
					when IDLE =>
						if ( START = '1' ) then
							current_state <= LOAD_X_0;
						else
							current_state <= IDLE;
						end if;
					when LOAD_X_0 =>
						current_state <= WAIT_X_0;
					when WAIT_X_0 =>
						if ( div_fin = '1' ) then
							current_state <= WASTE_1;
						else
							current_state <= WAIT_X_0;
						end if;
					when WASTE_1 =>
						current_state <= LOAD_Y_0;
					when LOAD_Y_0 =>
						current_state <= PROC_Y_0;
					when PROC_Y_0 =>
						current_state <= WAIT_Y_0;
					when WAIT_Y_0 =>
						if ( div_fin = '1' ) then
							current_state <= WASTE_2;
						else
							current_state <= WAIT_Y_0;
						end if;
					when WASTE_2 =>
						current_state <= SPIKE;
					when SPIKE =>
						current_state <= IDLE;
					when others =>
						current_state <= IDLE;
				end case;
			end if;
		end if;
	end process;

	process( current_state )
	begin
		case current_state is
			when IDLE =>
				select_signal <= '0';
				inverted_select_signal <= '0';
				div_start <= '0';
				FINISHED <= '0';
			when LOAD_X_0 =>
				select_signal <= '0';
				inverted_select_signal <= '1';
				div_start <= '1';
				FINISHED <= '0';
			when WAIT_X_0 =>
				select_signal <= '0';
				inverted_select_signal <= '1';
				div_start <= '0';
				FINISHED <= '0';
			when WASTE_1 =>
				select_signal <= '0';
				inverted_select_signal <= '1';
				div_start <= '0';
				FINISHED <= '0';
			when LOAD_Y_0 =>
				select_signal <= '1';
				inverted_select_signal <= '0';
				div_start <= '0';
				FINISHED <= '0';
			when PROC_Y_0 =>
				select_signal <= '1';
				inverted_select_signal <= '0';
				div_start <= '1';
				FINISHED <= '0';
			when WAIT_Y_0 =>
				select_signal <= '1';
				inverted_select_signal <= '0';
				div_start <= '0';
				FINISHED <= '0';
			when WASTE_2 =>
				select_signal <= '1';
				inverted_select_signal <= '0';
				div_start <= '0';
				FINISHED <= '0';
			when SPIKE =>
				select_signal <= '0';
				inverted_select_signal <= '0';
				div_start <= '0';
				FINISHED <= '1';
			when others =>
				select_signal <= '0';
				inverted_select_signal <= '0';
				div_start <= '0';
				FINISHED <= '0';
		end case;
	end process;

end architecture structural;


