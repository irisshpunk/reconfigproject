import math;

n = 16
i = 0
product = 1.0
current_term = 0.0
fixed_point_int = 0

while i < n:
	current_term = 1/math.sqrt(1 + math.pow( 2, -2*i ))
	product = product * current_term
	i = i + 1
	#print( str(i) + ": " + str(product) )
	# find the fixed point representation
	fixed_point_int = int(product * math.pow( 2, 16 ))
	print("scale_factor_LUT( " + str(i-1) + " ) <= \"" + ''.join(str((fixed_point_int>>j)&1) for j in xrange(31,-1,-1)) + "\";")

#print( product )
