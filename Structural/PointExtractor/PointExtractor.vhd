library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity PointExtractor is
	generic(
		DATA_IN_WIDTH : integer;
		NUMBER_OF_POINTS : integer := 256;
		BITS_PER_AXIS : integer := 16;
		PIXEL_WIDTH : integer := 8;
		SIMULTANEOUS_INST : integer
	);
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		FINISHED : out std_logic;
		START : in std_logic;
		-- coming from the verry top
		WIDTH : in std_logic_vector( 16 downto 1 );
		-- coming from the FIFO
		DATA_IN : in std_logic_vector( DATA_IN_WIDTH downto 1 );
		WIDE_FIFO_EMPTY : in std_logic;
		WIDE_FIFO_READ : out std_logic;
		-- out facing output
		DATA_OUT_L : out std_logic_vector( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
		DATA_OUT_R : out std_logic_vector( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
		-- SRAM control stuff
		SRAM_READ : out std_logic;
		SRAM_READ_FINISHED : in std_logic;
		SRAM_ADDRESS : out std_logic_vector( 22 downto 0 );
		SRAM_DATA_OUT : in std_logic_vector( 8 downto 1 );
		
		
		-- DEBUGGING
		LEDs : out std_logic_vector( 8 downto 1 )
	);
end entity;


architecture behavioral of PointExtractor is

-- type declarations
	type signal_array is array( natural range <> ) of std_logic_vector( 2 * BITS_PER_AXIS downto 1 );
	type state is ( IDLE, CHECK_FIFO, READ_FIFO, SET_CURRENT_SIGNAL, READ_SRAM, WAIT_READ_SRAM,
			PUSH_REG, CHECK_FOR_DONE_GROUP, CHECK_FOR_DONE_TOTAL, DONE, FIFO_WAIT );

-- component declarations

-- signal declarations
	signal broken_input : signal_array( SIMULTANEOUS_INST-1 downto 0 );
	signal current_value : std_logic_vector( 32 downto 1 );
	signal x : std_logic_vector( 16 downto 1 );
	signal y : std_logic_vector( 16 downto 1 );
	signal reg_out : std_logic_vector( 2 * NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
	signal reg_enable : std_logic;
	signal reg_in : std_logic_vector( 8 downto 1 );

	signal current_state : state := IDLE;
	signal short_counter : std_logic_vector( 16 downto 1 ) := ( others => '0' );
	signal long_counter : std_logic_vector( 16 downto 1 ) := ( others => '0' );

	signal long_address : std_logic_vector( 32 downto 1 );

	signal sLEDs : std_logic_vector( 8 downto 1 );

begin

-- component instantiations
	SRAM_ADDRESS <= long_address( 23 downto 1 );
	long_address <= std_logic_vector( unsigned( y ) * unsigned( WIDTH ) + unsigned( x ) );

-- concurrent signal assignments
	x <= current_value( 16 downto 1 );
	y <= current_value( 32 downto 17 );
	reg_in <= SRAM_DATA_OUT( 8 downto 1 );
	DATA_OUT_L <= reg_out( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
	DATA_OUT_R <= reg_out( 2 * NUMBER_OF_POINTS * PIXEL_WIDTH downto (NUMBER_OF_POINTS * PIXEL_WIDTH) + 1 );

	-- debugging
--	LEDs <= reg_out( 128 downto 121 );
	LEDs <= reg_out( 8 downto 1 );
--	LEDs <= sLEDs;

-- process blocks

	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( RESET = '1' ) then
				current_state <= IDLE;
			else
				case current_state is
					when IDLE =>
						sLEDs <= "00000000";
						short_counter <= ( others => '0' );
						long_counter <= ( others => '0' );
						if ( START = '1' ) then
							current_state <= CHECK_FIFO;
						else
							current_state <= IDLE;
						end if;
					when CHECK_FIFO =>
						sLEDs <= "00000001";
						if ( WIDE_FIFO_EMPTY = '0' ) then
							current_state <= READ_FIFO;
						else
							current_state <= CHECK_FIFO;
						end if;
					when READ_FIFO =>
						sLEDs <= "00000010";
						current_state <= FIFO_WAIT;
					when FIFO_WAIT =>
						sLEDs <= "00000011";
						current_state <= SET_CURRENT_SIGNAL;
					when SET_CURRENT_SIGNAL =>
						sLEDs <= "00000100";
						current_value <= broken_input( conv_integer( short_counter ) );
						current_state <= READ_SRAM;
					when READ_SRAM =>
						sLEDs <= "00000101";
						current_state <= WAIT_READ_SRAM;
					when WAIT_READ_SRAM =>
						sLEDs <= "00000110";
						if ( SRAM_READ_FINISHED = '1' ) then
							current_state <= PUSH_REG;
						else
							current_state <= WAIT_READ_SRAM;
						end if;
					when PUSH_REG =>
						sLEDs <= "00000111";
						current_state <= CHECK_FOR_DONE_GROUP;
					when CHECK_FOR_DONE_GROUP =>
						sLEDs <= "00001000";
						long_counter <= std_logic_vector( unsigned( long_counter ) + 1 );
						if ( unsigned( short_counter ) >= ( SIMULTANEOUS_INST - 1 ) ) then
							short_counter <= ( others => '0' );
							current_state <= CHECK_FOR_DONE_TOTAL;
						else
							short_counter <= std_logic_vector( unsigned( short_counter ) + 1 );
							current_state <= SET_CURRENT_SIGNAL;
						end if;
					when CHECK_FOR_DONE_TOTAL =>
						sLEDs <= "00001001";
						if ( unsigned( long_counter ) >= ( 2 * NUMBER_OF_POINTS - 1 ) ) then
							current_state <= DONE;
						else
							current_state <= CHECK_FIFO;
						end if;
					when DONE =>
						sLEDs <= "00001010";
						current_state <= IDLE;
--					when others =>
--						current_state <= IDLE;
				end case;
			end if;
		end if;
	end process;

	process( current_state )
	begin
		case current_state is
			when IDLE =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when CHECK_FIFO =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when READ_FIFO =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '1';
				reg_enable <= '0';
				FINISHED <= '0';
			when FIFO_WAIT =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when SET_CURRENT_SIGNAL =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when READ_SRAM =>
				SRAM_READ <= '1';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when WAIT_READ_SRAM =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when PUSH_REG =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '1';
				FINISHED <= '0';
			when CHECK_FOR_DONE_GROUP =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when CHECK_FOR_DONE_TOTAL =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '0';
			when DONE =>
				SRAM_READ <= '0';
				WIDE_FIFO_READ <= '0';
				reg_enable <= '0';
				FINISHED <= '1';
--			when others =>
--				SRAM_READ <= '0';
--				WIDE_FIFO_READ <= '0';
--				reg_enable <= '0';
--				FINISHED <= '0';
		end case;
	end process;

	process( DATA_IN )
	begin
		for i in 0 to SIMULTANEOUS_INST-1 loop
			broken_input( i ) <= DATA_IN( (i+1) * 2 * BITS_PER_AXIS downto ( 2 * (i) * BITS_PER_AXIS + 1) );
		end loop;
	end process;

	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( RESET = '1' ) then
				reg_out <= ( others => '0' );
			else
				if ( reg_enable = '1' ) then
					reg_out <= reg_in & reg_out( 2 * NUMBER_OF_POINTS * PIXEL_WIDTH downto 9 );
				end if;
			end if;
		end if;
	end process;

end architecture;

