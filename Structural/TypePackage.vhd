library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

package TypePackage is

	type pixel_column is array( integer range <> ) of std_logic_vector( 8 downto 1 );
	type point is array( 2 downto 1 ) of std_logic_vector( 8 downto 1 );
	type point_column is array( integer range <> ) of point;
	type wide_mux_input is array( integer range <>, integer range <> ) of std_logic;

end TypePackage;
