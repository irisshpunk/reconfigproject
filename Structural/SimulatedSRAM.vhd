library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

entity SimulatedSRAM is
	port(
		clk:	in std_logic;	-- clock
		addr:	in std_logic_vector(22 downto 0);	-- memory address
		adv:	in std_logic;	-- address valid
		ce:	in std_logic;	-- chip enable
		lb:	in std_logic;	-- lower byte
		ub:	in std_logic;	-- upper byte
		oe:	in std_logic;	-- output enable
		we:	in std_logic;	-- write enable
		dq:	inout std_logic_vector(15 downto 0);	-- data inputs/outputs
		w:		out std_logic	-- wait
	);
end SimulatedSRAM;

architecture Behavioral of SimulatedSRAM is
	-- 8 megs of 16 bits (128Mb)
	type mem is array (262144 downto 0) of std_logic_vector(15 downto 0);
	signal data : mem := (others => (others => '0'));
begin
	w <= '1';

	rd: process (oe)
	begin
	
		if (falling_edge(oe)) then
			dq <= data(to_integer(unsigned(addr))) after 2 ns;
		else
			dq <= (others => 'Z');
		end if;
	
	end process;
	
	wt: process (we)
	begin
	
		if (falling_edge(we)) then
			data(to_integer(unsigned(addr))) <= dq after 4500 ps;
		end if;
	
	end process;

end Behavioral;

