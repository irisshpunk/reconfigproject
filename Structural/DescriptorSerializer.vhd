library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity DescriptorSerializer is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;	
		FINISHED : out std_logic;
		DATA_IN : in std_logic_vector( 256 downto 1 );
		DATA_OUT : out std_logic_vector( 8 downto 1 );
		OUTPUT_WRITE : out std_logic;
		FIFO_FULL : in std_logic
	);
end entity;

architecture structural of DescriptorSerializer is

-- component declarations
COMPONENT BigMultiplexer is
	generic(
		SELECT_WIDTH : integer := 3;
		DATA_WIDTH : integer := 16
	);
	port(
		SELECT_LINE : in std_logic_vector( SELECT_WIDTH downto 1 );
		DATA_LINE : in std_logic_vector( (DATA_WIDTH * ((2**SELECT_WIDTH)))-1 downto 0 );
		DATA_OUT : out std_logic_vector( DATA_WIDTH downto 1 )
	);
end COMPONENT;

-- type declarations
	type state is ( IDLE, CHECK, WRITE, INC, DONE );

-- signal declarations
	signal current_state : state := IDLE;

	signal select_line_signal : std_logic_vector( 6 downto 1 );

begin

-- component instantiations
	BM : BigMultiplexer generic map( SELECT_WIDTH => 5, DATA_WIDTH => 8 )
			    port map( SELECT_LINE => select_line_signal( 5 downto 1 ),
				      DATA_LINE => DATA_IN, DATA_OUT => DATA_OUT );

-- concurrent signal assignments

-- process blocks
	process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
		case current_state is
			when IDLE =>
				select_line_signal <= ( others => '0' );
				FINISHED <= '0';
				OUTPUT_WRITE <= '0';
				if ( START = '1' ) then
					current_state <= CHECK;
				else
					current_state <= IDLE;
				end if;
			when CHECK =>
				OUTPUT_WRITE <= '0';
				FINISHED <= '0';
				if ( unsigned(select_line_signal) = 32 ) then
					current_state <= DONE;
				else
					current_state <= WRITE;
				end if;
			when WRITE =>
				OUTPUT_WRITE <= '1';
				FINISHED <= '0';
				current_state <= INC;
			when INC =>
				OUTPUT_WRITE <= '0';
				FINISHED <= '0';
				select_line_signal <= std_logic_vector( unsigned(select_line_signal) + 1 );
				current_state <= CHECK;
			when DONE =>
				OUTPUT_WRITE <= '0';
				FINISHED <= '1';
				current_state <= IDLE;
--			when others =>
--				current_state <= IDLE;
		end case;
		end if;
	end process;

end architecture;



