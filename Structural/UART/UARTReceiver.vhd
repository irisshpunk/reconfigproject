library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity UARTReceiver is
	generic (
		DBIT:		integer	:=	8;
		SB_TICK:	integer	:=	16
	);
	port (
		clk, reset:	in	std_logic;
		data_in:	in	std_logic;
		data_out:	out	std_logic_vector(7 downto 0);
		received:	out	std_logic
	);
end UARTReceiver;

architecture arch of UARTReceiver is

	type state_type is (idle, start, data, stop);
	signal current_s, next_s:	state_type;
	signal s, s_next:	unsigned(3 downto 0);
	signal n, n_next:	unsigned(2 downto 0);
	signal b, b_next:	std_logic_vector(7 downto 0);

	-- clk_div signals
	signal sclk_divider:	std_logic_vector(5 downto 0);
	signal sclk_enable:		std_logic;

begin

	-- Divide the clock to about 9600 Hz
	clk_div: process(clk, reset)
	begin
		if (reset = '1') then
			sclk_divider <= (others => '0');
			sclk_enable <= '0';
		elsif (clk'event and clk = '1') then
			-- if (sprev_data_in = not scurrent_data_in) then
				-- sclk_divider <= (others => '0');
				-- sclk_enable <= '1';		
			--elsif (sclk_divider = X"28B0") then
			--elsif (sclk_divider = X"364") then
			if (sclk_divider = X"36") then
				sclk_divider <= (others => '0');
				sclk_enable <= '1';
			else
				sclk_divider <= sclk_divider + 1;
				sclk_enable <= '0';
			end if;
		end if;
	end process;

	state_mach:	process (clk, reset)
	begin
		if (reset = '1') then
			current_s	<=	idle;
			s	<=	(others	=>	'0');
			n	<=	(others	=>	'0');
			b	<=	(others	=>	'0');
		elsif (rising_edge(clk)) then
			current_s	<=	next_s;
			s	<=	s_next;
			n	<=	n_next;
			b	<=	b_next;
		end if;
	end process;
	
	next_state:	process (current_s, s, n, b, sclk_enable, data_in)
	begin
		next_s	<=	current_s;
		s_next	<=	s;
		n_next	<=	n;
		b_next	<=	b;
		received	<=	'0';
		case current_s is
			when idle =>
				if (data_in = '0') then
					next_s	<=	start;
					s_next	<=	(others	=>	'0');
				end if;
			when start =>
				if (sclk_enable = '1') then
					if (s = 7) then
						next_s	<=	data;
						s_next	<=	(others	=>	'0');
						n_next	<=	(others	=>	'0');
					else
						s_next	<=	s + 1;
					end if;
				end if;
			when data =>
				if (sclk_enable = '1') then
					if (s = 15) then
						s_next	<=	(others	=>	'0');
						b_next	<=	data_in & b(7 downto 1);
						if ( n = (DBIT - 1)) then
							next_s	<=	stop;
						else
							n_next	<=	n + 1;
						end if;
					else
						s_next	<=	s + 1;
					end if;
				end if;
			when stop =>
				if (sclk_enable = '1') then
					if (s = (SB_TICK - 1)) then
						next_s	<=	idle;
						received	<= '1';
					else
						s_next	<=	s + 1;
					end if;
				end if;
		end case;
	end process;
	
	data_out	<=	b;
		
end arch;