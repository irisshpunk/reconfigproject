library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--------------------------------------------
-- UARTtransmitter:
-- This module sends data serially over a
-- a uart connection
--------------------------------------------
entity UARTtransmitter is
	port (	clk:			in std_logic;	-- clock
				reset:		in std_logic;	-- reset signal
				enable:		in std_logic;	-- enable the UARTtransmitter
				send:			in std_logic;	-- start sending data
				data_in:		in std_logic_vector(7 downto 0);	-- parallel data in
				data_out:	out std_logic;	-- serial data out
				sent:			out std_logic	-- indicate that the data has been sent
	);
end UARTtransmitter;

architecture behav of UARTtransmitter is
	-- clk_div signals
	signal sclk_divider : std_logic_vector(9 downto 0);
	signal sclk_enable : std_logic;

	-- State machine signals
	type state_type is (idle, start, send0, send1, send2, send3, send4, send5, send6, send7, stop);
	signal current_s, next_s: state_type;
begin
	-- Divide the clock to about 9600 Hz
	clk_div: process(clk, reset)
	begin
		if (reset = '1') then
			sclk_divider <= (others => '0');
			sclk_enable <= '0';
		elsif (rising_edge(clk)) then
			--if (sclk_divider = X"28B0") then
			if (sclk_divider = X"364") then
				sclk_divider <= (others => '0');
				sclk_enable <= '1';
			else
				sclk_divider <= sclk_divider + 1;
				sclk_enable <= '0';
			end if;
		end if;
	end process;

	switch_state: process(clk, reset)
	begin
		if (reset = '1') then
			current_s <= idle;
		elsif (rising_edge(clk)) then
			if (sclk_enable = '1') then
				if (enable = '1') then
					current_s <= next_s;
				end if;
			end if;
		end if;
	end process;
	
	state_machine: process(current_s, send)
	begin
		case current_s is
			when idle =>
				if (send = '1') then
					next_s <= start;
				else
					next_s <= idle;
				end if;
			when start =>
				next_s <= send0;
			when send0 =>
				next_s <= send1;
			when send1 =>
				next_s <= send2;
			when send2 =>
				next_s <= send3;
			when send3 =>
				next_s <= send4;
			when send4 =>
				next_s <= send5;
			when send5 =>
				next_s <= send6;
			when send6 =>
				next_s <= send7;
			when send7 =>
				next_s <= stop;
			when stop =>
				next_s <= idle;
		end case;
	end process;
	
	output: process(current_s, data_in)
	begin
		case current_s is
			when idle =>
				data_out <= '1';
				sent <= '0';
			when start =>
				data_out <= '0';
				sent <= '0';
			when send0 =>
				data_out <= data_in(0);
				sent <= '0';
			when send1 =>
				data_out <= data_in(1);
				sent <= '0';
			when send2 =>
				data_out <= data_in(2);
				sent <= '0';
			when send3 =>
				data_out <= data_in(3);
				sent <= '0';
			when send4 =>
				data_out <= data_in(4);
				sent <= '0';
			when send5 =>
				data_out <= data_in(5);
				sent <= '0';
			when send6 =>
				data_out <= data_in(6);
				sent <= '0';
			when send7 =>
				data_out <= data_in(7);
				sent <= '0';
			when stop =>
				data_out <= '1';
				sent <= '1';
		end case;
	end process;
end behav;