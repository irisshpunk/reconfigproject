-------------------------------------------------------------------------------
-- This testbench is used to read commands from an input file to help with
-- simulation.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity adv_tb is
end adv_tb;

architecture behav of adv_tb is

	-- Top level component holds the FPGA pieces
	component TopLevel
		port(
			LEDs	:	out	std_logic_vector(7 downto 0);
		
		-- 100 MHz  clock
		clock		:	in	std_logic;
		
		-- Reset signal
		reset		:	in	std_logic;
		
		-- UART input/output
		data_in		:	in	std_logic;
		data_out	:	out	std_logic;
		
		-- SRAM signals
		addr	:	out	std_logic_vector(22 downto 0);
		adv		:	out	std_logic;
		ce		:	out	std_logic;
		lb		:	out	std_logic;
		ub		:	out	std_logic;
		oe		:	out	std_logic;
		we		:	out	std_logic;
		dq		:	inout	std_logic_vector(15 downto 0);
		w		:	in	std_logic
		);
	end component;
	
	-- SimulatedSRAM simulates the SRAM chip
	component SimulatedSRAM is
		port(
			clk		:	in		std_logic;
			addr	:	in		std_logic_vector(22 downto 0);
			adv		:	in		std_logic;
			ce		:	in		std_logic;
			lb		:	in		std_logic;
			ub		:	in		std_logic;
			oe		:	in		std_logic;
			we		:	in		std_logic;
			dq		:	inout	std_logic_vector(15 downto 0);
			w		:	out		std_logic
		);
	end component;
	
	-- UART receiver component
	component UARTReceiver is
		generic (
			DBIT:		integer	:=	8;
			SB_TICK:	integer	:=	16
		);
		port(
			clk			:	in	std_logic;
			reset		:	in	std_logic;
			data_in		:	in	std_logic;
			data_out	:	out	std_logic_vector(7 downto 0);
			received	:	out	std_logic
		);
	end component;
	
	-- UART transmitter component
	component UARTTransmitter is
		port (	clk:		in std_logic;	-- clock
				reset:		in std_logic;	-- reset signal
				enable:		in std_logic;	-- enable the UARTTransmitter
				send:		in std_logic;	-- start sending data
				data_in:	in std_logic_vector(7 downto 0);	-- parallel data in
				data_out:	out std_logic;	-- serial data out
				sent:		out std_logic	-- indicate that the data has been sent
		);
	end component;
	
	-- FPGA signals
	signal	clk			:	std_logic						:=	'0';
	signal	rst			:	std_logic						:=	'0';
	signal	data_in		:	std_logic_vector(7 downto 0)	:= (others => '0');
	signal	data_out	:	std_logic_vector(7 downto 0)	:= (others => '0');
	signal	rx_data_in	:	std_logic						:=	'1';
	signal	tx_data_out	:	std_logic						:=	'1';
	
	-- SRAM signals
	signal	addr	:	std_logic_vector(22 downto 0)	:=	(others => '0');
	signal	adv		:	std_logic						:=	'1';
	signal	ce		:	std_logic						:=	'1';
	signal	lb		:	std_logic						:=	'1';
	signal	ub		:	std_logic						:=	'1';
	signal	oe		:	std_logic						:=	'1';
	signal	we		:	std_logic						:=	'1';
	signal	dq		:	std_logic_vector(15 downto 0)	:=	(others => '0');
	signal	w		:	std_logic						:=	'1';
	
	signal	received	:	std_logic;
	signal	send		:	std_logic	:=	'0';
	signal	sent		:	std_logic;
	
	-- Clock period definitions
   constant	clk_period	:	time	:=	1 ns;
   --constant baud_period :	time	:=	104166 ns;	-- 9600 baud
   constant	baud_period	:	time	:=	868 ns;
   
	-- Files
	file	cmd_file	:	TEXT	open	read_mode	is	"tb_cmds.txt";
	file	out_pgm		:	TEXT	open	write_mode	is	"out.pgm";
	file	in_pgm		:	TEXT	open	read_mode	is	"in.pgm";
	
	-- Image properties
	constant	rows		:	integer	:=	512;
	constant	cols		:	integer	:=	512;
	constant	max_val		:	integer	:=	255;
	constant	img_size	:	integer	:=	262144;
	
	-- Testbench memory
	type	mem		is	array(img_size - 1 downto 0) of integer	range 0 to 255;
	signal	data	:	mem	:=	(others => 0);
	
	signal	cycle_cnt	:	std_logic_vector(31 downto 0)	:=	(others	=>	'0');
	type descriptor_array is array(31 downto 0) of std_logic_vector(7 downto 0);
	signal descriptor : descriptor_array := (others => (others => '0'));
	
begin

	uut			:	TopLevel
		port map(
			clock		=>	clk,
			reset		=>	rst,
			data_in	=>	rx_data_in,
			data_out	=>	tx_data_out,
			
			addr	=>	addr,
			adv		=>	adv,
			ce		=>	ce,
			lb		=>	lb,
			ub		=>	ub,
			oe		=>	oe,
			we		=>	we,
			dq		=>	dq,
			w		=>	w
		);

	sim_sram	:	SimulatedSRAM
		port map(
			clk		=>	clk,
			addr	=>	addr,
			adv		=>	adv,
			ce		=>	ce,
			lb		=>	lb,
			ub		=>	ub,
			oe		=>	oe,
			we		=>	we,
			dq		=>	dq,
			w		=>	w
		);
		
	rx	:	UARTReceiver
		port map(
			clk			=>	clk,
			reset		=>	rst,
			data_in		=>	tx_data_out,
			data_out	=>	data_out,
			received	=>	received
		);
	
	tx	:	UARTTransmitter
		port map(	clk			=>	clk,
					reset		=>	rst,
					enable		=>	'1',
					send		=>	send,
					data_in		=>	data_in,
					data_out	=>	rx_data_in,
					sent		=>	sent
		);
	
	-- Clock process definitions
   clk_process :process
   begin
   
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
		
   end process;
   
	-- Stimulus process
	stim_proc: process
	
	variable	in_line	:	line;
	variable	in_cmd	:	string(1 to 8);
	variable	good_read	:	boolean;
	
	variable	out_line	:	line;
	variable	data_line	:	line;
	
	variable	pixel		:	integer	:=	0;
	variable	pixel_cnt	:	integer	:=	0;
	
	variable	size		:	std_logic_vector(31 downto 0);
	variable	r, c		:	std_logic_vector(15 downto 0);
	
	begin
   
		-- Hold reset state for 100 ns.
		rst <= '1';
		wait for 100 ns;
		rst <= '0';
		
		-- Wait for 10 clock cycles
		wait for clk_period * 10;
		
		-- Read the command file
		while (not endfile(cmd_file)) loop
		
			-- Read a line
			readline(cmd_file, in_line);
			
			-- Skip empty lines and comments
			while (in_line'length = 0 or in_line(1) = '#') loop
			
				readline(cmd_file, in_line);
			
			end loop;
			
			-- Read a command
			read(in_line, in_cmd);
			
			-- Switch on the command
			case in_cmd is
				when "READ_PGM" =>
					report "READ_PGM";
					wait for 1 ps;
					
					-- Skip the first line
					readline(in_pgm, in_line);
					
					-- Skip the rows, cols line
					readline(in_pgm, in_line);
					
					-- Skip the max value line
					readline(in_pgm, in_line);
					
					-- Read each line of the input pgm file
					while (not endfile(in_pgm)) loop
					
						-- Read the next line
						readline(in_pgm, in_line);
						
						-- Read the next value
						read(in_line, pixel, good_read);
						
						-- Read the values in the line
						while (good_read) loop
						
							-- report "Reading: " & Integer'image(pixel);
							-- report "Pixel Count: " & Integer'image(pixel_cnt);
							
							-- Assign the pixel
							data(pixel_cnt)	<=	pixel;
							
							-- Increment the pixel count
							pixel_cnt	:=	pixel_cnt + 1;
							
							-- If all the pixels have been read
							if (pixel_cnt >= img_size) then
								exit;
							end if;
						
							-- Read the next value
							read(in_line, pixel, good_read);
						
						end loop;
					
					end loop;
					
				when "SAVE_PGM" =>
					report "SAVE_PGM";
					wait for 1 ps;
					
					-- Write the first 3 lines
					write(out_line, string'("P2"));
					writeline(out_pgm, out_line);
					write(out_line,Integer'image(cols));
					write(out_line,string'(" "));
					write(out_line,Integer'image(rows));
					writeline(out_pgm, out_line );
					write(out_line, string'("255"));
					writeline(out_pgm, out_line);
					
					-- Write out data
					for i in 0 to rows - 1 loop
						for j in 0 to cols - 1 loop
						
							report "Writing: " & Integer'image(data(i * cols + j));
							report "From: " & Integer'image(i * cols + j);
							write(data_line, (data(i * cols + j)));
							write(data_line, string'(" "));
							
						end loop;
						writeline(out_pgm, data_line);
					end loop;
				
				when "SEND_PGM" =>
					report "SEND_PGM";
					
					-- Start by telling the FPGA that an image is coming
					-- Send command 0x01
					data_in	<=	std_logic_vector(to_unsigned(1, 8));
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- Send the number of rows
					-- byte 2
					r	:=	std_logic_vector(to_unsigned(rows, 16));
					data_in	<=	r(7 downto 0);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- byte 1	- 0
					data_in	<=	r(15 downto 8);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- Send the number of columns
					-- byte 2
					c	:=	std_logic_vector(to_unsigned(cols, 16));
					data_in	<=	c(7 downto 0);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- byte 1	- 0
					data_in	<=	c(15 downto 8);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					size	:=	std_logic_vector(to_unsigned(img_size, 32));
					
					-- Send the size of the image
					-- byte 4
					data_in	<=	size(7 downto 0);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- byte 3	- 
					data_in	<=	size(15 downto 8);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- byte 2
					data_in	<=	size(23 downto 16);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- byte 1
					data_in	<=	size(31 downto 24);
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- Send the data
					for i in 0 to rows - 1 loop
						for j in 0 to cols - 1 loop
						
							-- report "Sending: " & Integer'image(data(i * cols + j));
							-- report "From: " & Integer'image(i * cols + j);
							
							-- Get the pixel
							pixel := data(i * cols + j);
							data_in <= std_logic_vector(to_unsigned(pixel, 8));
							send	<=	'1';
							wait until sent = '1';
							send	<=	'0';
							wait until sent = '0';
							
						end loop;
					end loop;
					
					report "Finished Sending";
					
				when "REC_DESC" =>
					report "REC_DESC";
					--wait for baud_period;
					
					-- Start by asking the FPGA for the descriptor
					-- Send command 0x03
					data_in	<=	std_logic_vector(to_unsigned(3, 8));
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- Receive the data
					for i in 0 to 31 loop
					
						-- Receive the descriptor value one byte at a time
						wait until received = '1';
						
						descriptor(i)	<=	data_out;
						
						wait until received = '0';
						
					end loop;
					
					report "Finished Receiving";
				
				when "_COMPUTE"	=>
					report "_COMPUTE";
					
					-- Start median filter by telling the FPGA to start
					-- Send command 0x02
					data_in	<=	std_logic_vector(to_unsigned(2, 8));
					send	<=	'1';
					wait until sent = '1';
					send	<=	'0';
					wait until sent = '0';
					
					-- wait until received = '1';
					-- cycle_cnt(7 downto 0)	<=	data_out;
					-- wait until received = '0';
					
					-- wait until received = '1';
					-- cycle_cnt(15 downto 8)	<=	data_out;
					-- wait until received = '0';
					
					-- wait until received = '1';
					-- cycle_cnt(23 downto 16)	<=	data_out;
					-- wait until received = '0';
					
					-- wait until received = '1';
					-- cycle_cnt(31 downto 24)	<=	data_out;
					-- wait until received = '0';
				
				when others =>
					report "Unrecognised Command";
				
			end case;
		
		end loop;

		wait;
		
	end process;

end architecture;