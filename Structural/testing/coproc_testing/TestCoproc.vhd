library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TestCoproc is
	port(
		clock	:	in	std_logic;
		reset	:	in	std_logic;
		
		-- FIFO state signals
		fifo_in_empty	:	in	std_logic;
		fifo_out_full	:	in	std_logic;
		
		-- FIFO read and write signals
		fifo_in_rd	:	out	std_logic;
		fifo_out_wt	:	out	std_logic;
		
		-- FIFO input and output
		data_in		:	in	std_logic_vector(7 downto 0);
		data_out	:	out	std_logic_vector(7 downto 0);
		
		-- Memory read and write signals
		mem_rd	:	in	std_logic;
		mem_wt	:	out	std_logic;
		
		-- Memory input and output
		mem_data_in		:	in	std_logic_vector(15 downto 0);
		mem_data_out	:	out	std_logic_vector(15 downto 0);
		
		-- Memory address
		mem_addr	:	out	std_logic_vector(22 downto 0)
	);
end TestCoproc;

architecture behav of TestCoproc is

	-- State machine signals
	type state_type is (idle, loopback, send_output);
	signal	cs, ns	:	state_type	:=	idle;
	
	-- Detect new input
	signal	received, prev_received	:	std_logic	:=	'1';
	
	-- Store most recent input
	signal	input	:	std_logic_vector(7 downto 0)	:=	(others	=>	'0');
	
	COMPONENT div
	  PORT (
		aclk : IN STD_LOGIC;
		s_axis_divisor_tvalid : IN STD_LOGIC;
		s_axis_divisor_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		s_axis_dividend_tvalid : IN STD_LOGIC;
		s_axis_dividend_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		m_axis_dout_tvalid : OUT STD_LOGIC;
		m_axis_dout_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0)
	  );
	END COMPONENT;
	
	signal	s_axis_divisor_tvalid :	STD_LOGIC	:=	'1';
	signal	s_axis_divisor_tdata :	STD_LOGIC_VECTOR(31 DOWNTO 0)	:=	std_logic_vector(to_unsigned(9, 32));
	signal	s_axis_dividend_tvalid :	STD_LOGIC	:=	'1';
	signal	s_axis_dividend_tdata :	STD_LOGIC_VECTOR(31 DOWNTO 0)	:=	std_logic_vector(to_unsigned(3, 32));
	signal	m_axis_dout_tvalid :	STD_LOGIC;
	signal	m_axis_dout_tdata :	STD_LOGIC_VECTOR(63 DOWNTO 0);
	
begin

	your_instance_name : div
	  PORT MAP (
		aclk => clock,
		s_axis_divisor_tvalid => s_axis_divisor_tvalid,
		s_axis_divisor_tdata => s_axis_divisor_tdata,
		s_axis_dividend_tvalid => s_axis_dividend_tvalid,
		s_axis_dividend_tdata => s_axis_dividend_tdata,
		m_axis_dout_tvalid => m_axis_dout_tvalid,
		m_axis_dout_tdata => m_axis_dout_tdata
	  );

	-- Read when the input FIFO is not empty
	fifo_in_rd	<=	not fifo_in_empty;
	
	-- A value has been loaded into the co-processor if the input FIFO is empty
	received	<=	fifo_in_empty;
	
	-- Write to registers on rising edge
	eval:	process(clock, reset)
	begin
	
		if (reset = '1') then
		
			-- Set the current state to idle
			cs	<=	idle;
			
			-- Reset input register
			input	<=	(others	=>	'0');
			
			-- Reset the FIFO output
			data_out	<=	(others	=>	'0');
			
			-- Stop writing to the output FIFO
			fifo_out_wt	<=	'0';
		
		elsif (rising_edge(clock)) then
		
			-- Set the current state
			cs	<=	ns;
			
			-- Set the most recent input
			input	<=	data_in;
			
			-- Detect new input
			prev_received	<=	received;
			
			-- State machine evaluation
			case	cs	is
			
				-- Sit here and do nothing
				when	idle	=>
				
					-- Stop writing to the output FIFO
					fifo_out_wt	<=	'0';
					
					-- Reset the FIFO output
					data_out	<=	(others	=>	'0');
				
				when	loopback	=>
				
					data_out	<=	input;
					fifo_out_wt	<=	'1';
				
				when	send_output	=>
				
					data_out	<=	m_axis_dout_tdata(7 downto 0);
					fifo_out_wt	<=	'1';
			
			end case;
			
		end if;
	
	end process;

	-- Next State
	sm:	process(cs, received, prev_received, input)
	begin
	
		case	cs	is
		
			-- Idle state when nothing is happening
			when	idle	=>
			
				-- Detect if a command has been received
				if (received = '1' and prev_received = '0') then
				
					ns	<=	loopback;
				
				elsif(m_axis_dout_tvalid = '1') then
				
					ns	<=	send_output;
				
				else
				
					ns	<=	idle;
				
				end if;
			
			when	loopback	=>
			
				-- Detect if a command has been received
				if (received = '1' and prev_received = '0') then
				
					ns	<=	loopback;
				
				else
				
					ns	<=	idle;
				
				end if;
			
			when	send_output	=>
			
				ns	<=	idle;
			
		end case;
	
	end process;
	
end behav;