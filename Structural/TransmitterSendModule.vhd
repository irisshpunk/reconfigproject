----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:27:31 10/16/2013 
-- Design Name: 
-- Module Name:    TransmitterSendModule - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TransmitterSendModule is
	PORT (
		CLOCK : in std_logic;
		RESET : in std_logic;
		FIFO_EMPTY : in std_logic;
		TRANS_SEND : out std_logic;
		FIFO_READ : out std_logic
	);
end TransmitterSendModule;

architecture Behavioral of TransmitterSendModule is

	signal counter : std_logic_vector( 24 downto 1 ) := (others => '0');

	type state is ( IDLE, FIFO_NOT_EMPTY, FIFO_EMPTY_STATE, WAIT_STATE, WAIT_STATE_2, WAIT_STATE_3,
							WAIT_STATE_4, WAIT_STATE_5,
							FINAL_STATE_1, FINAL_STATE_2, FINAL_STATE_3 );
	signal current_state : state := IDLE;

begin

	process ( CLOCK, RESET ) 
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( RESET = '1' ) then
				current_state <= IDLE;
			else
			case current_state is
				when IDLE =>
					if ( FIFO_EMPTY = '0' ) then
						current_state <= WAIT_STATE;
					else
						current_state <= IDLE;
					end if;
				when WAIT_STATE =>
					current_state <= WAIT_STATE_2;
				when WAIT_STATE_2 =>
					current_state <= WAIT_STATE_3;
				when WAIT_STATE_3 =>
					current_state <= WAIT_STATE_4;
				when WAIT_STATE_4 =>
					current_state <= WAIT_STATE_5;
				when WAIT_STATE_5 =>
					current_state <= FIFO_NOT_EMPTY;
				when FIFO_NOT_EMPTY =>
--					if ( FIFO_EMPTY = '1' ) then
						current_state <= FIFO_EMPTY_STATE;
--					else
--						current_state <= FIFO_NOT_EMPTY;
--					end if;
				when FIFO_EMPTY_STATE =>
					if ( unsigned(counter) = 41664 ) then
						current_state <= FINAL_STATE_1;
					else
						current_state <= FIFO_EMPTY_STATE;
					end if;
				when FINAL_STATE_1 =>
					current_state <= FINAL_STATE_2;
				when FINAL_STATE_2 =>
					current_state <= FINAL_STATE_3;
				when FINAL_STATE_3 =>
					if ( unsigned(counter) = 396672 ) then
						current_state <= IDLE;
					else
						current_state <= FINAL_STATE_3;
					end if;
				when others =>
					current_state <= IDLE;
			end case;
			end if;
		end if;
	end process;

	process ( current_state ) 
	begin
		case current_state is
			when IDLE =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when WAIT_STATE =>
				TRANS_SEND <= '0';
				FIFO_READ <= '1';
			when WAIT_STATE_2 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '1';
			when WAIT_STATE_3 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when WAIT_STATE_4 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when WAIT_STATE_5 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when FIFO_NOT_EMPTY =>
				TRANS_SEND <= '1';
				FIFO_READ <= '0';
			when FIFO_EMPTY_STATE =>
				TRANS_SEND <= '1';
				FIFO_READ <= '0';
			when FINAL_STATE_1 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when FINAL_STATE_2 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when FINAL_STATE_3 =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
			when others =>
				TRANS_SEND <= '0';
				FIFO_READ <= '0';
		end case;
	end process;

	clock_divider: process( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( current_state = FIFO_EMPTY_STATE or current_state = FINAL_STATE_3 ) then
				counter <= std_logic_vector(unsigned(counter) + 1);
			else
				counter <= (others => '0');
			end if;
		end if;
	end process clock_divider;

end Behavioral;

