library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.TypePackage.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity ORBCoprocessor is
	GENERIC(
		DESCRIPTOR_LENGTH : integer := 256
	);
	PORT (
		LEDs : out std_logic_vector(7 downto 0);
		CLOCK : in std_logic;
		DATA_IN : in std_logic_vector( 8 downto 1 );
		DATA_OUT : out std_logic_vector( 8 downto 1 );
		IN_FIFO_READ : out std_logic;
		IN_FIFO_EMPTY : in std_logic;
		OUT_FIFO_FULL : in std_logic;
		WRITE_SIGNAL : out std_logic;
		RESET : in std_logic;
		
		MEM_ADDRESS : out std_logic_vector( 22 downto 0 );
		MEM_READ : out std_logic;
		MEM_WRITE : out std_logic;
		MEM_DATA_IN : out std_logic_vector( 7 downto 0 );
		MEM_DATA_OUT : in std_logic_vector( 7 downto 0 );
		MEM_READ_FINISHED : in std_logic;
		MEM_WRITE_FINISHED : in std_logic
	);
end ORBCoprocessor;

architecture structural of ORBCoprocessor is

COMPONENT Summator is 
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		ENABLE : in std_logic;
		DATA_IN : in std_logic_vector( 8 downto 1 );
		x : in std_logic_vector( 16 downto 1 );
		y : in std_logic_vector( 16 downto 1 );
		m_00 : out std_logic_vector( 32 downto 1 );
		m_01 : out std_logic_vector( 64 downto 1 );
		m_10 : out std_logic_vector( 64 downto 1 )
	);
end COMPONENT;

COMPONENT WidePixelComparator is
	generic(
		DESCRIPTOR_LENGTH : integer := 256
	);
	port(
		DATA_IN_LEFT : in pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_IN_RIGHT : in pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_OUT : out std_logic_vector( DESCRIPTOR_LENGTH downto 1 )
	);
end COMPONENT;

COMPONENT WidePointRotator is
	generic(
		SIMULTANEOUS_INSTANCES_HIGH : integer;
		SELECT_WIDTH_HIGH : integer;
		NUMBER_OF_ITERATIONS : integer;
		BITS_PER_AXIS_HIGH : integer := 16
	);
	port(
		LEDs : out std_logic_vector(7 downto 0);
		m_10 : in std_logic_vector( 64 downto 1 );
		m_01 : in std_logic_vector( 64 downto 1 );
		m_00 : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FIFO_FULL : in std_logic;
		FINISHED : out std_logic;
		WRITE_SIGNAL : out std_logic;
		DATA_OUT : out std_logic_vector( 2 * SIMULTANEOUS_INSTANCES_HIGH * BITS_PER_AXIS_HIGH downto 1 )
	);
end COMPONENT;

COMPONENT WideFIFO is
	generic (
		depth : integer;
		width : integer;
		simultaneous_inst : integer;
		bits_per_axis : integer := 16;
		block_inst : integer
	);
	port (
		clk : in std_logic;
		rst : in std_logic;
		wr : in std_logic;
		rd : in std_logic;
		d_in : in std_logic_vector ( (2 * simultaneous_inst * bits_per_axis) - 1 downto 0 );
		f_full : out std_logic;
		f_empty : out std_logic;
		words_in : out std_logic_vector (width - 1 downto 0);
		d_out : out std_logic_vector ( (2 * simultaneous_inst * bits_per_axis)-1 downto 0);
		-- debugging
		tick : out std_logic_vector( width - 1 downto 0 )
	);
end COMPONENT;

COMPONENT PointExtractor is
	generic(
		DATA_IN_WIDTH : integer;
		NUMBER_OF_POINTS : integer := 256;
		BITS_PER_AXIS : integer := 16;
		PIXEL_WIDTH : integer := 8;
		SIMULTANEOUS_INST : integer
	);
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		FINISHED : out std_logic;
		START : in std_logic;
		-- coming from the verry top
		WIDTH : in std_logic_vector( 16 downto 1 );
		-- coming from the FIFO
		DATA_IN : in std_logic_vector( DATA_IN_WIDTH downto 1 );
		WIDE_FIFO_EMPTY : in std_logic;
		WIDE_FIFO_READ : out std_logic;
		-- out facing output
		DATA_OUT_L : out std_logic_vector( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
		DATA_OUT_R : out std_logic_vector( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
		-- SRAM control stuff
		SRAM_READ : out std_logic;
		SRAM_READ_FINISHED : in std_logic;
		SRAM_ADDRESS : out std_logic_vector( 22 downto 0 );
		SRAM_DATA_OUT : in std_logic_vector( 8 downto 1 );
		
		
		-- DEBUGGING
		LEDs : out std_logic_vector( 8 downto 1 )
	);
end COMPONENT;


COMPONENT DescriptorSerializer is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;	
		FINISHED : out std_logic;
		DATA_IN : in std_logic_vector( 256 downto 1 );
		DATA_OUT : out std_logic_vector( 8 downto 1 );
		OUTPUT_WRITE : out std_logic;
		FIFO_FULL : in std_logic
	);
end COMPONENT;

	-- state machines
	type state_type is (idle, read_command,
						receive_image,
						get_rows_1, get_rows_2,
						get_cols_1, get_cols_2,
						get_size_1, get_size_2, get_size_3, get_size_4,
						start_get_image,
						get_pixel,
						save_to_memory, wait_for_save_to_memory,
						start_describing, wait_for_describer,
						send_descriptor
						);
	signal cs, ns : state_type := idle;

	-- signal declarations

	signal WPR_START : std_logic := '0';
	signal WPR_FIN : std_logic;
	signal PE_START : std_logic := '0';
	signal PE_FIN : std_logic;
	signal wide_fifo_full : std_logic;
	signal wide_fifo_empty : std_logic;
	signal wide_fifo_write : std_logic := '0';
	signal wide_fifo_read : std_logic := '0';
	signal PE_OUT_L : std_logic_vector( 256 * 8 downto 1 );
	signal PE_OUT_R : std_logic_vector( 256 * 8 downto 1 );
--	signal wide_fifo_in : std_logic_vector( ( 2 * 16 * 16 ) - 1 downto 0 );
--	signal wide_fifo_out : std_logic_vector( ( 2 * 16 * 16 ) - 1 downto 0 );
	signal wide_fifo_in : std_logic_vector( ( 2 * 16 * 4 ) - 1 downto 0 );
	signal wide_fifo_out : std_logic_vector( ( 2 * 16 * 4 ) - 1 downto 0 );
	signal serializer_start : std_logic := '0';

	signal fifo_received, previous_fifo_received : std_logic := '1';
	signal img_rows : std_logic_vector(15 downto 0) := (others => '0');
	signal img_cols : std_logic_vector(15 downto 0) := (others => '0');
	signal current_column : std_logic_vector(15 downto 0) := (others => '0');
	signal current_row : std_logic_vector(15 downto 0) := (others => '0');
	signal img_size : std_logic_vector(31 downto 0) := (others => '0');
	signal memory_write_index : std_logic_vector(22 downto 0) := (others => '0');
	
	signal SRAMInput : std_logic_vector( 15 downto 0 ) := ( others => '0' );
	signal SRAMOutput : std_logic_vector( 15 downto 0 ) := ( others => '0' );
	signal SRAMAddress : std_logic_vector( 22 downto 0 ) := ( others => '0' );
	signal SRAMWrite : std_logic := '0';
--	signal SRAMRead : std_logic := '0';
	signal SRAMReadFinished : std_logic;
	signal SRAMWriteFinished : std_logic;

	signal x : std_logic_vector( 16 downto 1 ) := ( others => '0' );
	signal y : std_logic_vector( 16 downto 1 ) := ( others => '0' );
	signal m_00 : std_logic_vector( 32 downto 1 );
	signal m_01 : std_logic_vector( 64 downto 1 );
	signal m_10 : std_logic_vector( 64 downto 1 );
	signal sum_enable : std_logic := '0';

	signal ORB_descriptor : std_logic_vector( DESCRIPTOR_LENGTH downto 1 );

	signal left_pixels : pixel_column( DESCRIPTOR_LENGTH downto 1 );
	signal right_pixels : pixel_column( DESCRIPTOR_LENGTH downto 1 );
	
	signal PE_driving : std_logic := '0';
	signal PE_ADDRESS : std_logic_vector( 22 downto 0 );
	signal PE_READ : std_logic;
	
begin

	-- components
	Sum : Summator port map( CLOCK => CLOCK, RESET => RESET, ENABLE => sum_enable, DATA_IN => DATA_IN, x => x, y => y, m_00 => m_00, m_01 => m_01, m_10 => m_10 );

	Desc : WidePixelComparator generic map( DESCRIPTOR_LENGTH => DESCRIPTOR_LENGTH ) port map( DATA_IN_LEFT => left_pixels, DATA_IN_RIGHT => right_pixels, DATA_OUT => ORB_descriptor );

	WPR : WidePointRotator --generic map( 16, 5, 32, 16 )
					     -- this is the number of point rotators
				generic map( SIMULTANEOUS_INSTANCES_HIGH => 4, 
					     -- this should be log_2( 512/( number of point rotators ) )
					     SELECT_WIDTH_HIGH => 7, 
					     -- this should be 512 / ( number of point rotators )
					     NUMBER_OF_ITERATIONS => 128, 
					     -- this should pretty much always stay 16
					     BITS_PER_AXIS_HIGH => 16 )
				port map( LEDs => LEDs, 
						m_10 => m_10, m_01 => m_01, m_00 => m_00,
					  CLOCK => CLOCK, RESET => RESET, START => WPR_START,
					  WIDTH => img_cols, HEIGHT => img_rows, fifo_full => wide_fifo_full,
					  FINISHED => WPR_FIN, WRITE_SIGNAL => wide_fifo_write,
					  DATA_OUT => wide_fifo_in );

				      -- these constants are held over from before
	WFIFO : WideFIFO generic map( depth => 32, width => 6, 
				      -- this is the number of point rotators
				      simultaneous_inst => 4, 
				      -- this should be fixed at 16
				      bits_per_axis => 16,
				     -- this should be ( number of rotators ) * ( bits per axis ) * 2 / 8
				     block_inst => 16 )
--			generic map( depth => 32, width => 6, simultaneous_inst => 16, bits_per_axis => 16,
--				     block_inst => 64 )
			port map( CLK => CLOCK, rst => RESET, wr => wide_fifo_write, rd => wide_fifo_read,
				  d_in => wide_fifo_in, f_full => wide_fifo_full, f_empty => wide_fifo_empty,
				  d_out => wide_fifo_out );

					 -- this is 2 * bits_per_axis * ( number of rotators )
	PE : PointExtractor generic map( DATA_IN_WIDTH => 2*16*4, 
					 -- this should pretty much always be 256
					 NUMBER_OF_POINTS => 256, 
					 -- this should be 16
					 BITS_PER_AXIS => 16,
					 -- this should be 8 all the time 
					 PIXEL_WIDTH => 8, 
					 -- this is the number of rotators
					 SIMULTANEOUS_INST => 4 )
--			      generic map( 2*16*16, 256, 16, 8, 16 )
			      port map( CLOCK => CLOCK, RESET => RESET, FINISHED => pe_fin,
					START => PE_START, WIDTH => img_cols, DATA_IN => wide_fifo_out,
					WIDE_FIFO_EMPTY => wide_fifo_empty, WIDE_FIFO_READ => wide_fifo_read,
					DATA_OUT_L => PE_OUT_L, DATA_OUT_R => PE_OUT_R, 
					SRAM_READ => PE_READ, SRAM_READ_FINISHED => SRAMReadFinished,
					SRAM_ADDRESS => PE_ADDRESS, SRAM_DATA_OUT => MEM_DATA_OUT
					);--, LEDs => LEDs );

	S3r14l1z3r : DescriptorSerializer port map( CLOCK => CLOCK, RESET => RESET, START => serializer_start,
						    DATA_IN => ORB_DESCRIPTOR, DATA_OUT => DATA_OUT, 
						    OUTPUT_WRITE => WRITE_SIGNAL, FIFO_FULL => OUT_FIFO_FULL );


	-- concurrent signal assignments
	MEM_DATA_IN <= SRAMInput(7 downto 0);
	SRAMOutput(7 downto 0) <= MEM_DATA_OUT;
	MEM_ADDRESS <= SRAMAddress when PE_driving = '0' else PE_ADDRESS;
	MEM_WRITE <= SRAMWrite;
--	MEM_READ <= SRAMRead or PE_READ;
	MEM_READ <= PE_READ;
	SRAMReadFinished <= MEM_READ_FINISHED;
	SRAMWriteFinished <= MEM_WRITE_FINISHED;
	IN_FIFO_READ <= not IN_FIFO_EMPTY;
	fifo_received <= IN_FIFO_EMPTY;
	-- debugging
--	LEDs <= PE_OUT_R( 128 downto 121 );

	-- process blocks
	process( PE_OUT_L, PE_OUT_R )
	begin
		for i in 1 to 256 loop
			left_pixels( i ) <= PE_OUT_L( 8*i downto 8*(i-1) + 1 );
			right_pixels( i ) <= PE_OUT_R( 8*i downto 8*(i-1) + 1 );
		end loop;
	end process;

	eval: process(clock, reset)
	begin
	
		if (rising_edge(clock)) then
		
			if (reset = '1') then
			
				SRAMInput <= (others => '0');
				SRAMAddress <= (others => '0');
				WPR_START <= '0';
				PE_START <= '0';
				
				cs <= idle;
				
			else
		
				-- Set the current state
				cs	<=	ns;
				
				-- Detect new input
				previous_fifo_received	<=	fifo_received;
				
				case cs is
					when idle =>
						PE_DRIVING <= '1';
						SRAMInput <= (others => '0');
						SRAMAddress <= (others => '0');
						WPR_START <= '0';
						PE_START <= '0';
						serializer_start <= '0';
						
					when read_command =>
						
						PE_DRIVING <= '0';
					when receive_image =>
						img_rows	<=	(others	=>	'0');
						img_cols	<=	(others	=>	'0');
						img_size	<=	(others	=>	'0');
						memory_write_index	<=	(others	=>	'0');
						current_row	<=	std_logic_vector(to_unsigned(1, 16));
						current_column	<=	std_logic_vector(to_unsigned(1, 16));
						PE_DRIVING <= '0';
					when get_rows_1 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							img_rows(7 downto 0)	<=	data_in;
						end if;
					when get_rows_2 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							img_rows(15 downto 8)	<=	data_in;
						end if;
					when get_cols_1 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then

							-- This was removed so that the current_row could be 1 indexed, not that it matters
							-- img_rows	<=	std_logic_vector(unsigned(img_rows) - 1);
							img_cols(7 downto 0)	<=	data_in;
						end if;
					when get_cols_2 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							img_cols(15 downto 8)	<=	data_in;
						end if;
					when get_size_1 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							-- This was removed so that the current_column could be 1 indexed
							-- img_cols	<=	std_logic_vector(unsigned(img_cols) - 1);
							img_size(7 downto 0)	<=	data_in;
						end if;
					when get_size_2 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							img_size(15 downto 8)	<=	data_in;
						end if;
					when get_size_3 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							img_size(23 downto 16)	<=	data_in;
						end if;
					when get_size_4 =>
						PE_DRIVING <= '0';
						if (previous_fifo_received = '0' and fifo_received = '1') then
							img_size(31 downto 24)	<=	data_in;
						end if;
					when start_get_image =>
						PE_DRIVING <= '0';
						img_size	<=	std_logic_vector(unsigned(img_size) - 1);
					when get_pixel =>
						PE_DRIVING <= '0';
						if (unsigned(memory_write_index) /= unsigned(img_size) + 1) then
							if (previous_fifo_received = '0' and fifo_received = '1') then
								SRAMAddress	<=	memory_write_index;
								SRAMInput(7 downto 0)	<=	data_in;
								memory_write_index	<=	std_logic_vector(unsigned(memory_write_index) + 1);
								sum_enable	<=	'1';
								x	<=	current_column;
								y	<=	current_row;
								if (current_column = img_cols) then
									current_column <= std_logic_vector(to_unsigned(1, 16));
									current_row <= std_logic_vector(unsigned(current_row) + 1);
								else
									current_column <= std_logic_vector(unsigned(current_column) + 1);
								end if;
							end if;
						end if;
					when save_to_memory =>
						PE_DRIVING <= '0';
						SRAMWrite	<=	'1';
						sum_enable	<=	'0';
					when wait_for_save_to_memory =>
						PE_DRIVING <= '0';
						SRAMWrite	<=	'0';
					when start_describing =>
						PE_DRIVING <= '1';
						WPR_START <= '1';
						PE_START <= '1';
					when wait_for_describer =>
						PE_DRIVING <= '1';
						WPR_START <= '0';
						PE_START <= '0';
					when send_descriptor =>
						PE_DRIVING <= '1';
						serializer_start <= '1';
				end case;
				
			end if;
		
		end if;
		
	end process;
	
	sm: process(cs, previous_fifo_received, fifo_received, MEM_WRITE_FINISHED, memory_write_index, img_size, data_in, sramwritefinished, pe_fin)
	begin
	
		case cs is
			when idle =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= read_command;
				else
					ns <= idle;
				end if;
			when read_command =>
				if (data_in = X"01") then
					ns <= receive_image;
				elsif (data_in = X"02") then
					ns <= start_describing;
				elsif (data_in = X"03") then
					ns <= send_descriptor;
				else
					ns <= idle;
				end if;
			when receive_image =>
				ns <= get_rows_1;
			when get_rows_1 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_rows_2;
				else
					ns <= get_rows_1;
				end if;
			when get_rows_2 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_cols_1;
				else
					ns <= get_rows_2;
				end if;
			when get_cols_1 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_cols_2;
				else
					ns <= get_cols_1;
				end if;
			when get_cols_2 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_size_1;
				else
					ns <= get_cols_2;
				end if;
			when get_size_1 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_size_2;
				else
					ns <= get_size_1;
				end if;
			when get_size_2 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_size_3;
				else
					ns <= get_size_2;
				end if;
			when get_size_3 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= get_size_4;
				else
					ns <= get_size_3;
				end if;
			when get_size_4 =>
				if (previous_fifo_received = '0' and fifo_received = '1') then
					ns <= start_get_image;
				else
					ns <= get_size_4;
				end if;
			when start_get_image =>
				ns <= get_pixel;
			when get_pixel =>
				if (unsigned(memory_write_index) = unsigned(img_size) + 1) then
					ns <= idle;
				else
					if (previous_fifo_received = '0' and fifo_received = '1') then
						ns <= save_to_memory;
					else
						ns <= get_pixel;
					end if;
				end if;
			when save_to_memory =>
				ns <= wait_for_save_to_memory;
			when wait_for_save_to_memory =>
				if (SRAMWriteFinished = '1') then
					ns <= get_pixel;
				else
					ns <= wait_for_save_to_memory;
				end if;
			when start_describing =>
				ns <= wait_for_describer;
			when wait_for_describer =>
				if (PE_FIN = '1') then
					ns <= idle;
				else
					ns <= wait_for_describer;
				end if;
			when send_descriptor =>
				ns <= idle;
		end case;
	
	end process;
	
end architecture structural;
