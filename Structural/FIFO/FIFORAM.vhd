----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:01:29 10/15/2013 
-- Design Name: 
-- Module Name:    FIFORAM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FIFORAM is
	generic(
		data_width : integer;
		addr_width : integer
	);
	port (
		CLOCK : in std_logic;
		addr : in std_logic_vector( addr_width - 1 downto 0 );
		wr : in std_logic;
		data_in : in std_logic_vector( data_width - 1 downto 0 );
		data_out : out std_logic_vector( data_width - 1 downto 0 )
	);
end FIFORAM;

architecture Behavioral of FIFORAM is
	type mem_type is array ( (2**addr_width)-1 downto 0 ) of std_logic_vector(data_width-1 downto 0);
	signal memory : mem_type;
begin

	process ( CLOCK )
	begin
		if ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( wr = '1' ) then
				memory( conv_integer(addr) ) <= data_in;
			end if;
			data_out <= memory( conv_integer(addr) );
		end if;
	end process;

end Behavioral;

