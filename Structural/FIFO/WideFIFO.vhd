----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:40:08 10/15/2013 
-- Design Name: 
-- Module Name:    CounterBasedFIFO - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity WideFIFO is
	generic (
		depth : integer;
		width : integer;
		simultaneous_inst : integer;
		bits_per_axis : integer := 16;
		block_inst : integer
	);
	port (
		clk : in std_logic;
		rst : in std_logic;
		wr : in std_logic;
		rd : in std_logic;
		d_in : in std_logic_vector ( (2 * simultaneous_inst * bits_per_axis) - 1 downto 0 );
		f_full : out std_logic;
		f_empty : out std_logic;
		words_in : out std_logic_vector (width - 1 downto 0);
		d_out : out std_logic_vector ( (2 * simultaneous_inst * bits_per_axis)-1 downto 0);
		-- debugging
		tick : out std_logic_vector( width - 1 downto 0 )
	);
end WideFIFO;

architecture Behavioral of WideFIFO is

	signal internal_word_count : std_logic_vector( width - 1 downto 0 ) := (others => '0');
	signal internal_empty : std_logic;
	signal read_pointer : std_logic_vector( width - 1 downto 0 ) := (others => '0');
	signal write_pointer : std_logic_vector( width - 1 downto 0 ) := (others => '0');
	signal internal_mem_address : std_logic_vector( width - 1 downto 0 );
	signal internal_d_in : std_logic_vector( 7 downto 0 ) := (others => '0');
	signal internal_wr : std_logic;
	signal internal_d_out : std_logic_vector ( (2 * simultaneous_inst * bits_per_axis) - 1 downto 0);

	component FIFORAM is
		generic(
			data_width : integer;
			addr_width : integer
		);
		port (
			CLOCK : in std_logic;
			addr : in std_logic_vector( addr_width - 1 downto 0 );
			wr : in std_logic;
			data_in : in std_logic_vector( data_width - 1 downto 0 );
			data_out : out std_logic_vector( data_width - 1 downto 0 )
		);
	end component FIFORAM;

	type state is ( IDLE, R, W, RW, R_2, R_3, W_2, RW_2 );
	signal current_state : state := IDLE;

	signal second_counter : std_logic_vector( width-1 downto 0 ) := (others => '0');
--	signal state_mem : std_logic_vector( 7 downto 0 ) := ( others => '0' );
	signal x : std_logic_vector ( 1 downto 0 );

begin

-- component declaration

	REP_GEN:
	for i in 1 to block_inst generate
		RAM_BLOCK : FIFORAM GENERIC MAP ( DATA_WIDTH => 8, ADDR_WIDTH => width )
				PORT MAP ( CLOCK => clk, addr => internal_mem_address, wr => internal_wr, 
				data_in => d_in( (i*8) - 1 downto ((i-1)*8) ), 
				data_out => internal_d_out( (i*8) - 1 downto ((i-1)*8) ) );
	end generate;

	words_in <= internal_word_count;
--	f_empty <= '1' when read_pointer = write_pointer else
--					'0';
	f_full <= '1' when to_integer(unsigned(internal_word_count)) = (depth-1) else
				 '0';
	x <= rd & wr;
	--internal_mem_address <= read_pointer when rd = '1' else
	--								write_pointer when wr = '1' else
	--								read_pointer; -- This should really be something else

--	-- handle counting words
--	process ( clk )
--		variable new_word_count : std_logic_vector( width - 1 downto 0 ) := (others => '0');
--	begin
--		if ( rst = '1' ) then
--			internal_word_count <= (others => '0');
--			read_pointer <= (others => '0');
--			write_pointer <= (others => '0');
--		else
--			if ( clk'EVENT and clk = '1' ) then
--				if ( wr = '1' ) then
--					new_word_count := std_logic_vector(unsigned(internal_word_count) + 1);
--					write_pointer <= std_logic_vector(unsigned(write_pointer) + 1);
--				end if;
--				if ( rd = '1' ) then
--					new_word_count := std_logic_vector(unsigned(internal_word_count) - 1);
--					read_pointer <= std_logic_vector(unsigned(read_pointer) + 1);
--				end if;
--				internal_word_count <= new_word_count;
--			end if;
--		end if;
--	end process;

	tick <= write_pointer;

	process ( CLK, RST )
		--variable x : std_logic_vector( 1 downto 0 );
	begin
		--x := rd & wr;
		if ( RST = '1' ) then
				current_state <= IDLE;
--				internal_d_in <= (others => '0');
			else
		if ( CLK'EVENT and CLK = '1' ) then
				case current_state is
					when IDLE =>
						case x is
							when "00" =>
								--current_state <= IDLE;
							when "01" =>
								-- write
								current_state <= W;
								-- latch
--								internal_d_in <= d_in;
							when "10" =>
								-- read
								current_state <= R;
								-- latch
								--internal_d_in <= d_in;
							when "11" =>
								-- do both!
								current_state <= RW;
								-- latch
--								internal_d_in <= d_in;
							when others =>
								current_state <= IDLE;
						end case;
--						state_mem <= "00000000";
					when R =>
						current_state <= R_2;
--						state_mem <= "00000001";
						--second_counter <= std_logic_vector(unsigned(second_counter) + 1); -- appears to happen the correct number of times
					when R_2 =>
						current_state <= IDLE;
--						state_mem <= "00000010";
					when R_3 =>
						current_state <= IDLE;
--						state_mem <= "00000011";
--						d_out <= internal_d_out;
					when W =>
--						if ( rd = '1' ) then
--							current_state <= R;
--						else
							current_state <= IDLE;
--						end if;
--						state_mem <= "00000100";
--						second_counter <= std_logic_vector(unsigned(second_counter) + 1);
					when RW =>
						current_state <= RW_2;
--						state_mem <= "00000101";
					when RW_2 =>
						current_state <= R;
--						state_mem <= "00000110";
--						second_counter <= std_logic_vector(unsigned(second_counter) + 1);
					when others =>
						current_state <= IDLE;
				end case;
			end if;
		end if;
	end process;

	process( current_state, read_pointer, write_pointer )
	begin
--		if ( CLK'EVENT and CLK = '1' ) then
		case current_state is
			when IDLE =>
				internal_mem_address <= read_pointer;
				if ( write_pointer = read_pointer ) then
					f_empty <= '1';
				else
					f_empty <= '0';
				end if;
				internal_wr <= '0';
			when R =>
				internal_mem_address <= read_pointer;
--				read_pointer <= std_logic_vector(unsigned(read_pointer) + 1);
--				internal_word_count <= std_logic_vector(unsigned(internal_word_count) - 1);
--				d_out <= internal_d_out;
				internal_wr <= '0';
			when R_2 =>
				--d_out <= internal_d_out;
				internal_wr <= '0';
			when R_3 =>
				internal_wr <= '0';
			when W =>
				internal_mem_address <= write_pointer;
--				write_pointer <= std_logic_vector(unsigned(write_pointer) + 1);
--				internal_word_count <= std_logic_vector(unsigned(internal_word_count) + 1);
				internal_wr <= '1';
			when RW =>
				internal_mem_address <= write_pointer;
--				write_pointer <= std_logic_vector(unsigned(write_pointer) + 1);
--				internal_word_count <= std_logic_vector(unsigned(internal_word_count) + 1);
				internal_wr <= '0';
			when RW_2 =>
				internal_mem_address <= write_pointer;
				internal_wr <= '1';
			when others =>	
				internal_mem_address <= read_pointer;
				internal_wr <= '0';
		end case;
--		end if;
	end process;

	process ( CLK, RST )
		variable new_word_count : std_logic_vector( width-1 downto 0 ) := (others => '0');
	begin
		if ( RST = '1' ) then
			write_pointer <= (others => '0');
			read_pointer <= (others => '0');
			internal_word_count <= (others => '0');
			d_out <= (others => '0');
--			second_counter <= (others => '0');
		else
			if ( CLK'EVENT and CLK = '1' ) then
				if ( current_state = W or current_state = RW ) then
					write_pointer <= std_logic_vector(unsigned(write_pointer) + 1);
					new_word_count := std_logic_vector(unsigned(internal_word_count) + 1);
				end if;
--				if ( current_state = R ) then
--					d_out <= internal_d_out;
--					second_counter <= std_logic_vector(unsigned(second_counter) + 1);
--				end if;
				if ( current_state = R_2 ) then
--					d_out <= internal_d_out;
					read_pointer <= std_logic_vector(unsigned(read_pointer) + 1);
					new_word_count := std_logic_vector(unsigned(internal_word_count) - 1);
				end if;
				internal_word_count <= new_word_count;
			end if;
			if ( CLK'EVENT and CLK = '0' ) then
				if ( current_state = R ) then
					d_out <= internal_d_out;
					second_counter <= std_logic_vector(unsigned(second_counter) + 1);
				end if;
			end if;
		end if;
	end process;

end Behavioral;

