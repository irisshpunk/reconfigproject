library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CounterBasedFIFO is
	generic (
		depth : integer := 256;
		width : integer := 8
	);
	port (
		clk : in std_logic;
		rst : in std_logic;
		wr : in std_logic;
		rd : in std_logic;
		d_in : in std_logic_vector (7 downto 0);
		f_full : out std_logic;
		f_empty : out std_logic;
		words_in : out integer range 0 to depth - 1;
		d_out : out std_logic_vector (7 downto 0)
		);
end CounterBasedFIFO;

architecture behav of CounterBasedFIFO is
type mem is array (depth - 1 downto 0) of std_logic_vector(7 downto 0);
signal data : mem := (others => (others => '0'));
signal ptread, ptwrite : integer range 0 to depth - 1;
signal s_words_in : integer range 0 to depth - 1;
signal s_f_full, s_f_empty : std_logic;
begin

	words_in <= s_words_in;
	f_full <= s_f_full;
	f_empty <= s_f_empty;
	
	check: process (rst, ptread, ptwrite, s_words_in)
	begin
		if (rst = '1') then
			s_f_full <= '0';
			s_f_empty <= '0';
		elsif(s_words_in = depth - 1) then
			s_f_full <= '1';
			s_f_empty <= '0';
		elsif(s_words_in = 0) then
			s_f_full <= '0';
			s_f_empty <= '1';
		else
			s_f_full <= '0';
			s_f_empty <= '0';
		end if;
	end process;
	
	f_rd: process (clk, rst)
	begin
		if (rising_edge(clk)) then
			if (rst = '1') then
				d_out <= (others => '0');
			elsif (rd = '1' and s_f_empty = '0') then
				d_out <= data(ptread);
			end if;
		end if;
	end process;
	
	f_wr: process (clk, rst)
	begin
		if (rst = '1') then
		elsif (rising_edge(clk)) then
			if (wr = '1' and s_f_full = '0') then
				data(ptwrite) <= d_in;
			end if;
		end if;
	end process;
	
	chg_ptrs: process (clk, rst)
	begin
		if (rst = '1') then
			ptread <= 0;
			ptwrite <= 0;
			s_words_in <= 0;
		elsif (rising_edge(clk)) then
			if (rd = '1' and s_f_empty = '0') then
				if (ptread = depth - 1) then
					ptread <= 0;
				else
					ptread <= ptread + 1;
				end if;
			end if;
			if (wr = '1' and s_f_full = '0') then
				if (ptwrite = depth - 1) then
					ptwrite <= 0;
				else
					ptwrite <= ptwrite + 1;
				end if;
			end if;
			if (rd = '0' and wr = '1' and s_f_full = '0') then
				s_words_in <= s_words_in + 1;
			elsif (rd = '1' and wr = '0' and s_f_empty = '0') then
				s_words_in <= s_words_in - 1;
			else
				s_words_in <= s_words_in;
			end if;
		end if;
	end process;

end behav;