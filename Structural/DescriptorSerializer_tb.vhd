library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity DescriptorSerializer_tb is
end entity;

architecture testbench of DescriptorSerializer_tb is

-- component declarations
COMPONENT DescriptorSerializer is
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;	
		FINISHED : out std_logic;
		DATA_IN : in std_logic_vector( 256 downto 1 );
		DATA_OUT : out std_logic_vector( 8 downto 1 );
		OUTPUT_WRITE : out std_logic;
		FIFO_FULL : in std_logic
	);
end COMPONENT;

-- signal declarations
signal CLOCK : std_logic;
signal RESET : std_logic;
signal START : std_logic;
signal FINISHED : std_logic;
signal DATA_IN : std_logic_vector( 256 downto 1 );
signal DATA_OUT : std_logic_vector( 8 downto 1 );
signal OUTPUT_WRITE : std_logic;
signal FIFO_FULL : std_logic := '0';

begin

-- component instantiations
uut : DescriptorSerializer port map( CLOCK => CLOCK, RESET => RESET, START => START, FINISHED => FINISHED, DATA_IN => DATA_IN, DATA_OUT => DATA_OUT, OUTPUT_WRITE => OUTPUT_WRITE, FIFO_FULL => FIFO_FULL );

-- concurrent signal assignments

-- process blocks

	-- stimulus
	process
	begin
		RESET <= '1';
		START <= '0';
		wait for 100 ns;
		RESET <= '0';
		wait for 50 ns;
		DATA_IN <= ( others => '0' );
		wait for 10 ns;
		DATA_IN( 256 downto 249 ) <= "01010101";
		DATA_IN( 240 downto 233 ) <= "11110001";
		DATA_IN( 32 downto 25 ) <= "11110000";
		DATA_IN( 24 downto 17 ) <= "00000111";
		DATA_IN( 8 downto 1 ) <= "11111111";
		wait for 40 ns;
		START <= '1';
		wait for 500 ns;
		wait;
	end process;

	-- clock gen
	process
	begin
		CLOCK <= '0';
		wait for 5 ns;
		CLOCK <= '1';
		wait for 5 ns;
	end process;


end architecture testbench;

 
