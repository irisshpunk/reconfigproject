library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity Summator is 
	port(
		CLOCK : in std_logic;
		ENABLE : in std_logic;
		RESET : in std_logic;
		DATA_IN : in std_logic_vector( 8 downto 1 );
		x : in std_logic_vector( 16 downto 1 );
		y : in std_logic_vector( 16 downto 1 );
		m_00 : out std_logic_vector( 32 downto 1 );
		m_01 : out std_logic_vector( 64 downto 1 );
		m_10 : out std_logic_vector( 64 downto 1 )
	);
end entity;

architecture behavioral of Summator is
	signal m_00_signal : std_logic_vector( 32 downto 1 ) := ( others => '0' );
	signal m_01_signal : std_logic_vector( 64 downto 1 ) := ( others => '0' );
	signal m_10_signal : std_logic_vector( 64 downto 1 ) := ( others => '0' );
begin

	m_00 <= m_00_signal;
	m_01 <= m_01_signal;
	m_10 <= m_10_signal;


	-- very simple
	process( CLOCK, RESET )
	begin
		if ( RESET = '1' ) then
			m_00_signal	<=	(others	=>	'0');
			m_10_signal	<=	(others	=>	'0');
			m_01_signal	<=	(others	=>	'0');
		elsif ( CLOCK'EVENT and CLOCK = '1' ) then
			if ( ENABLE = '1' ) then
				m_00_signal <= std_logic_vector( unsigned(m_00_signal) + unsigned(DATA_IN) );
				m_10_signal <= std_logic_vector( unsigned(m_10_signal) + ( unsigned(DATA_IN) * unsigned(x) ) );
				m_01_signal <= std_logic_vector( unsigned(m_01_signal) + ( unsigned(DATA_IN) * unsigned(y) ) );
			end if;
		end if;
	end process;


end architecture;


