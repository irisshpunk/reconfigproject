library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity WidePointRotExtr_tb is
end entity;


architecture tb of WidePointRotExtr_tb is

-- component declarations
COMPONENT WidePointRotator is
	generic(
		SIMULTANEOUS_INSTANCES_HIGH : integer;
		SELECT_WIDTH_HIGH : integer;
		NUMBER_OF_ITERATIONS : integer;
		BITS_PER_AXIS_HIGH : integer := 16
	);
	port(
		m_10 : in std_logic_vector( 64 downto 1 );
		m_01 : in std_logic_vector( 64 downto 1 );
		m_00 : in std_logic_vector( 32 downto 1 );
		CLOCK : in std_logic;
		RESET : in std_logic;
		START : in std_logic;
		WIDTH : in std_logic_vector( 16 downto 1 );
		HEIGHT : in std_logic_vector( 16 downto 1 );
		FIFO_FULL : in std_logic;
		FINISHED : out std_logic;
		WRITE_SIGNAL : out std_logic;
		DATA_OUT : out std_logic_vector( 2 * SIMULTANEOUS_INSTANCES_HIGH * BITS_PER_AXIS_HIGH downto 1 )
	);
end COMPONENT;

COMPONENT PointExtractor is
	generic(
		DATA_IN_WIDTH : integer;
		NUMBER_OF_POINTS : integer := 256;
		BITS_PER_AXIS : integer := 16;
		PIXEL_WIDTH : integer := 8;
		SIMULTANEOUS_INST : integer
	);
	port(
		CLOCK : in std_logic;
		RESET : in std_logic;
		FINISHED : out std_logic;
		START : in std_logic;
		-- coming from the verry top
		WIDTH : in std_logic_vector( 16 downto 1 );
		-- coming from the FIFO
		DATA_IN : in std_logic_vector( DATA_IN_WIDTH downto 1 );
		WIDE_FIFO_EMPTY : in std_logic;
		WIDE_FIFO_READ : out std_logic;
		-- out facing output
		DATA_OUT_L : out std_logic_vector( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
		DATA_OUT_R : out std_logic_vector( NUMBER_OF_POINTS * PIXEL_WIDTH downto 1 );
		-- SRAM control stuff
		SRAM_READ : out std_logic;
		SRAM_READ_FINISHED : in std_logic;
		SRAM_ADDRESS : out std_logic_vector( 22 downto 0 );
		SRAM_DATA_OUT : in std_logic_vector( 8 downto 1 )
	);
end COMPONENT;

COMPONENT WideFIFO is
	generic (
		depth : integer;
		width : integer;
		simultaneous_inst : integer;
		bits_per_axis : integer := 16;
		block_inst : integer
	);
	port (
		clk : in std_logic;
		rst : in std_logic;
		wr : in std_logic;
		rd : in std_logic;
		d_in : in std_logic_vector ( (2 * simultaneous_inst * bits_per_axis) - 1 downto 0 );
		f_full : out std_logic;
		f_empty : out std_logic;
		words_in : out std_logic_vector (width - 1 downto 0);
		d_out : out std_logic_vector ( (2 * simultaneous_inst * bits_per_axis) downto 0);
		-- debugging
		tick : out std_logic_vector( width - 1 downto 0 )
	);
end COMPONENT;

-- signal declarations
	signal CLOCK : std_logic := '0';
	signal RESET : std_logic := '0';
	signal SRAM_DATA_OUT : std_logic_vector( 8 downto 1 );
	signal SRAM_READ : std_logic;
	signal READ_FINISHED : std_logic;
	signal ADDRESS : std_logic_vector( 22 downto 0 );
	signal m_10 : std_logic_vector( 64 downto 1 );
	signal m_01 : std_logic_vector( 64 downto 1 );
	signal m_00 : std_logic_vector( 32 downto 1 );
	signal START : std_logic;
	signal wpr_fin : std_logic;
	signal pe_fin : std_logic;
	signal width : std_logic_vector( 16 downto 1 );
	signal height : std_logic_vector( 16 downto 1 );
	signal fifo_full : std_logic;
	signal fifo_write : std_logic;
--	signal wpr_data_out : std_logic_vector( 2 * 16 * 16 downto 1 );
	signal wpr_data_out : std_logic_vector( 32 downto 1 );
	signal fifo_empty : std_logic;
	signal fifo_read : std_logic;
	signal pe_out_l : std_logic_vector( 256 * 8 downto 1 );
	signal pe_out_r : std_logic_vector( 256 * 8 downto 1 );
--	signal fifo_data_in : std_logic_vector( (2 * 16 * 16) - 1 downto 0 );
	signal fifo_data_in : std_logic_vector( 31 downto 0 );
--	signal fifo_data_out : std_logic_vector( (2 * 16 * 16) - 1 downto 0 );
	signal fifo_data_out : std_logic_vector( 31 downto 0 );


	signal axis_1_tb : std_logic_vector( 16 downto 1 );
	signal axis_2_tb : std_logic_vector( 16 downto 1 );

begin

-- component instantations
	uut1 : WidePointRotator generic map( 1, 9, 256, 16 )
--				generic map( 16, 5, 32, 16 )
				port map( m_10 => m_10, m_01 => m_01, m_00 => m_00,
					  CLOCK => CLOCK, RESET => RESET, START => START,
					  WIDTH => width, HEIGHT => height, fifo_full => fifo_full,
					  FINISHED => wpr_fin, WRITE_SIGNAL => fifo_write,
					  DATA_OUT => wpr_data_out );

	uut2 : PointExtractor generic map( 2*16, 256, 16, 8, 1 )
--			      generic map( 2*16*16, 256, 16, 8, 16 )
			      port map( CLOCK => CLOCK, RESET => RESET, FINISHED => pe_fin,
					START => START, WIDTH => width, DATA_IN => wpr_data_out,
					WIDE_FIFO_EMPTY => fifo_empty, WIDE_FIFO_READ => fifo_read,
					DATA_OUT_L => pe_out_l, DATA_OUT_R => pe_out_r, 
					SRAM_READ => SRAM_READ, SRAM_READ_FINISHED => READ_FINISHED,
					SRAM_ADDRESS => ADDRESS, SRAM_DATA_OUT => SRAM_DATA_OUT );

	uut3 : WideFIFO generic map( depth => 1024, width => 16, simultaneous_inst => 1, bits_per_axis => 16,
				     block_inst => 4 )
--			generic map( depth => 32, width => 6, simultaneous_inst => 16, bits_per_axis => 16,
--				     block_inst => 64 )
			port map( CLK => CLOCK, rst => RESET, wr => fifo_write, rd => fifo_read,
				  d_in => wpr_data_out, f_full => fifo_full, f_empty => fifo_empty,
				  d_out => fifo_data_out );

-- concurrent signal assignments
	SRAM_DATA_OUT <= "01011010";
	READ_FINISHED <= '1';
	WIDTH <= "00000010" & "00000000";
	HEIGHT <= "00000010" & "00000000";
	axis_1_tb <= wpr_data_out( 16 downto 1 );
	axis_2_tb <= wpr_data_out( 32 downto 17 );

-- clock generator
	process
	begin
		CLOCK <= '0';
		wait for 5 ns;
		CLOCK <= '1';
		wait for 5 ns;
	end process;

-- stimulus
	process
	begin
		RESET <= '1';
		START <= '0';
		--m_10 <= "00000000" & "00001010" & "00000000" & "00000000";
		m_10 <= "00000000" & "00000000" & "00000000" & "00000010" &
			"00000101" & "10111001" & "11010111" & "10000000";
		--m_01 <= "00000000" & "00000101" & "00000000" & "00000000";
		m_01 <= "00000000" & "00000000" & "00000000" & "00000001" &
			"11100000" & "10111001" & "00101111" & "10000000";
		--m_00 <= "00000000" & "00000000" & "00100001" & "00000000";
		m_00 <= "00000001" & "11101111" & "01010011" & "11100000";
		wait for 100 ns;
		RESET <= '0';
		wait for 30 ns;
		START <= '1';
		wait;
		
	end process;
	

end architecture;

