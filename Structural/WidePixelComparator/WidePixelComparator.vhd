library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.TypePackage.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity WidePixelComparator is
	generic(
		DESCRIPTOR_LENGTH : integer := 256
	);
	port(
		DATA_IN_LEFT : in pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_IN_RIGHT : in pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_OUT : out std_logic_vector( DESCRIPTOR_LENGTH downto 1 )
	);
end entity;

architecture structural of WidePixelComparator is

COMPONENT SinglePixelComparator is
	port(
		DATA_IN_LEFT : in std_logic_vector( 8 downto 1 );
		DATA_IN_RIGHT : in std_logic_vector( 8 downto 1 );
		DATA_OUT : out std_logic
	);
end COMPONENT;

begin

	PIX_COMP : 
	for i in 1 to DESCRIPTOR_LENGTH generate
		SINGLE_PIX : SinglePixelComparator port map( DATA_IN_LEFT => DATA_IN_LEFT( i ), DATA_IN_RIGHT => DATA_IN_RIGHT( i ), DATA_OUT => DATA_OUT( i ) );
	end generate PIX_COMP;

end architecture;


