library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity SinglePixelComparator is
	port(
		DATA_IN_LEFT : in std_logic_vector( 8 downto 1 );
		DATA_IN_RIGHT : in std_logic_vector( 8 downto 1 );
		DATA_OUT : out std_logic
	);
end entity;

architecture behavioral of SinglePixelComparator is
begin

	DATA_OUT <= '1' when (unsigned(DATA_IN_LEFT) < unsigned(DATA_IN_RIGHT)) ELSE
		    '0';

end architecture;
