library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Top level
entity TopLevel is
	port(
		LEDs	:	out	std_logic_vector(7 downto 0);
		
		-- 100 MHz  clock
		clock		:	in	std_logic;
		
		-- Reset signal
		reset		:	in	std_logic;
		
		-- UART input/output
		data_in		:	in	std_logic;
		data_out	:	out	std_logic;
		
		-- SRAM signals
		addr	:	out	std_logic_vector(22 downto 0);
		adv		:	out	std_logic;
		ce		:	out	std_logic;
		lb		:	out	std_logic;
		ub		:	out	std_logic;
		oe		:	out	std_logic;
		we		:	out	std_logic;
		dq		:	inout	std_logic_vector(15 downto 0);
		w		:	in	std_logic
	);
end TopLevel;

architecture arch of TopLevel is

	-- Component declarations
	
	-- FIFO
	component CounterBasedFIFO is
		generic(
			depth	:	integer	:=	256;
			width	:	integer	:=	8
		);
		port(
			clk			:	in	std_logic;
			rst			:	in	std_logic;
			wr			:	in	std_logic;
			rd			:	in	std_logic;
			d_in		:	in	std_logic_vector(7 downto 0);
			f_full		:	out	std_logic;
			f_empty		:	out	std_logic;
			words_in	:	out	integer	range 0 to depth - 1;
			d_out		:	out	std_logic_vector(7 downto 0)
		);
	end component;
	
	-- UART components
	
	-- UART receiver
	component UARTReceiver is
		generic(
			DBIT	:	integer	:=	8;
			SB_TICK	:	integer	:=	16
		);
		port(
			clk			:	in	std_logic;
			reset		:	in	std_logic;
			data_in		:	in	std_logic;
			data_out	:	out	std_logic_vector(7 downto 0);
			received	:	out	std_logic
		);
	end component;
	
	-- UART transmitter
	component UARTTransmitter is
		port(
			clk			:	in	std_logic;
			reset		:	in	std_logic;
			enable		:	in	std_logic;
			send		:	in	std_logic;
			data_in		:	in	std_logic_vector(7 downto 0);
			data_out	:	out	std_logic;
			sent		:	out	std_logic
		);
	end component;
	
	-- Co-processor
	component ORBCoprocessor is
		GENERIC(
			DESCRIPTOR_LENGTH : integer := 256
		);
		PORT (
			LEDs : out std_logic_vector(7 downto 0);
			CLOCK : in std_logic;
			DATA_IN : in std_logic_vector( 8 downto 1 );
			DATA_OUT : out std_logic_vector( 8 downto 1 );
			IN_FIFO_READ : out std_logic;
			IN_FIFO_EMPTY : in std_logic;
			OUT_FIFO_FULL : in std_logic;
			WRITE_SIGNAL : out std_logic;
			RESET : in std_logic;
			
			MEM_ADDRESS : out std_logic_vector( 22 downto 0 );
			MEM_READ : out std_logic;
			MEM_WRITE : out std_logic;
			MEM_DATA_IN : out std_logic_vector( 7 downto 0 );
			MEM_DATA_OUT : in std_logic_vector( 7 downto 0 );
			MEM_READ_FINISHED : in std_logic;
			MEM_WRITE_FINISHED : in std_logic
		);
	end component;
	-- component TestCoproc is
		-- port(
			-- clock	:	in	std_logic;
			-- reset	:	in	std_logic;
			
			-- FIFO state signals
			-- fifo_in_empty	:	in	std_logic;
			-- fifo_out_full	:	in	std_logic;
			
			-- FIFO read and write signals
			-- fifo_in_rd	:	out	std_logic;
			-- fifo_out_wt	:	out	std_logic;
			
			-- FIFO input and output
			-- data_in		:	in	std_logic_vector(7 downto 0);
			-- data_out	:	out	std_logic_vector(7 downto 0);
			
			-- Memory read and write signals
			-- mem_rd	:	in	std_logic;
			-- mem_wt	:	out	std_logic;
			
			-- Memory input and output
			-- mem_data_in		:	in	std_logic_vector(15 downto 0);
			-- mem_data_out	:	out	std_logic_vector(15 downto 0);
			
			-- Memory address
			-- mem_addr	:	out	std_logic_vector(22 downto 0)
		-- );
	-- end component;
	
	-- SRAM controller
	component SRAMController is
		port(
			CLOCK : in std_logic;
			RESET : in std_logic;
			DATA_IN : in std_logic_vector( 8 downto 1 );
			DATA_OUT : out std_logic_vector( 8 downto 1 );
			C : out std_logic_vector( 8 downto 1 );
			SRAM_WRITE : in std_logic;
			SRAM_READ : in std_logic;
			READ_FINISHED : out std_logic;
			WRITE_FINISHED : out std_logic;
			ADDRESS : in std_logic_vector( 22 downto 0 );
			CE_N : out std_logic;
			WE_N : out std_logic;
			OE_N : out std_logic;
			ADV_N : out std_logic;
			CRE : out std_logic;
			DQ : inout std_logic_vector( 15 downto 0 );
			LB_N : out std_logic;
			UB_N : out std_logic;
			busy : in std_logic;
			OUT_ADDRESS : out std_logic_vector( 22 downto 0 )
		);
	end component;
	
	-- Receive FIFO signals
	signal	fifo_rx_wr			:	std_logic;
	signal	fifo_rx_rd			:	std_logic;
	signal	fifo_rx_d_in		:	std_logic_vector(7 downto 0);
	signal	fifo_rx_full		:	std_logic;
	signal	fifo_rx_empty		:	std_logic;
	signal	fifo_rx_words_in	:	integer range 0 to 128 - 1;
	signal	fifo_rx_d_out		:	std_logic_vector(7 downto 0);
	
	-- Transmit FIFO signals
	signal	fifo_tx_wr			:	std_logic;
	signal	fifo_tx_rd			:	std_logic;
	signal	fifo_tx_d_in		:	std_logic_vector(7 downto 0);
	signal	fifo_tx_full		:	std_logic;
	signal	fifo_tx_empty		:	std_logic;
	signal	fifo_tx_words_in	:	integer range 0 to 128 - 1;
	signal	fifo_tx_d_out		:	std_logic_vector(7 downto 0);
	
	-- UART Receiver signals
	signal	rx_data_in	:	std_logic;
	signal	rx_data_out	:	std_logic_vector(7 downto 0);
	signal	rx_received	:	std_logic;
	
	-- UART Transmitter signals
	-- signal	tx_enable	:	std_logic;
	signal	tx_send		:	std_logic;
	signal	tx_data_in	:	std_logic_vector(7 downto 0);
	signal	tx_data_out	:	std_logic;
	signal	tx_sent		:	std_logic;
	
	-- TODO: These might be moved into a transmitter controller
	signal	tx_prev_sent	:	std_logic	:=	'0';
	signal	tx_prev_send	:	std_logic	:=	'0';
	signal	send			:	std_logic	:=	'0';
	signal	prev_send		:	std_logic	:=	'0';
	
	-- Co-processor signals
	signal	cp_fifo_in_empty		:	std_logic;
	signal	cp_fifo_out_full		:	std_logic;
	signal	cp_fifo_rd				:	std_logic;
	signal	cp_fifo_wt				:	std_logic;
	signal	cp_data_in				:	std_logic_vector(7 downto 0);
	signal	cp_data_out				:	std_logic_vector(7 downto 0);
	signal	cp_mem_rd				:	std_logic;
	signal	cp_mem_wt				:	std_logic;
	signal	cp_mem_data_in			:	std_logic_vector(7 downto 0);
	signal	cp_mem_data_out			:	std_logic_vector(7 downto 0);
	signal	cp_mem_addr				:	std_logic_vector(22 downto 0);
	signal	cp_mem_write_finished	:	std_logic;
	signal	cp_mem_read_finished	:	std_logic;
	
	-- SRAM Controller
	signal	sram_address		:	std_logic_vector(22 downto 0);
	signal	sram_rd				:	std_logic;
	signal	sram_wt				:	std_logic;
	signal	sram_data_in		:	std_logic_vector(7 downto 0);
	signal	sram_data_out		:	std_logic_vector(7 downto 0);
	signal	sram_write_finished	:	std_logic;
	signal	sram_read_finished	:	std_logic;

begin
--	LEDs	<=	fifo_rx_d_out;
	-- Combinational logic
	
	-- Receive FIFO
	fifo_rx_wr	<=	rx_received;
	fifo_rx_rd	<=	cp_fifo_rd;
	fifo_rx_d_in	<=	rx_data_out;
	
	-- Write FIFO
	fifo_tx_wr		<=	cp_fifo_wt;
	-- this might move to a transmitter controller
	fifo_tx_rd		<=	(tx_send and (not tx_prev_send)) or ((not tx_sent) and tx_prev_sent);
	fifo_tx_d_in	<=	cp_data_out;
	
	-- UART rx
	rx_data_in	<=	data_in;
	
	-- UART tx
	-- tx_enable	<=	'1';
	tx_data_in	<=	fifo_tx_d_out;
	-- this might move to a transmitter controller
	tx_send		<=	send and prev_send;
	data_out	<=	tx_data_out;
	
	-- Co-processor
	cp_fifo_in_empty		<=	fifo_rx_empty;
	cp_fifo_out_full		<=	fifo_tx_full;
	cp_data_in				<=	fifo_rx_d_out;
	cp_mem_write_finished	<=	sram_write_finished;
	cp_mem_read_finished	<=	sram_read_finished;
	
	-- SRAM Controller
	sram_address	<=	cp_mem_addr;
	sram_rd			<=	cp_mem_rd;
	sram_wt			<=	cp_mem_wt;
	cp_mem_data_out	<=	sram_data_out;
	sram_data_in	<=	cp_mem_data_in;
	
	-- Receive FIFO
	fifo_rx:	CounterBasedFIFO
		generic map(
			depth	=>	128,
			width	=>	7
		)
		port map(
			clk		=>	clock,
			rst		=>	reset,
			wr		=>	fifo_rx_wr,
			rd		=>	fifo_rx_rd,
			d_in	=>	fifo_rx_d_in,
			f_full	=>	fifo_rx_full,
			f_empty	=>	fifo_rx_empty,
			-- words_in	=>	,
			d_out	=>	fifo_rx_d_out
		);
	
	-- Transmit FIFO
	fifo_tx:	CounterBasedFIFO
		generic map(
			depth	=>	128,
			width	=>	7
		)
		port map(
			clk		=>	clock,
			rst		=>	reset,
			wr		=>	fifo_tx_wr,
			rd		=>	fifo_tx_rd,
			d_in	=>	fifo_tx_d_in,
			f_full	=>	fifo_tx_full,
			f_empty	=>	fifo_tx_empty,
			-- words_in	=>	,
			d_out	=>	fifo_tx_d_out
		);
	
	-- UART Receiver
	rx:	UARTReceiver
		port map(
			clk			=>	clock,
			reset		=>	reset,
			data_in		=>	rx_data_in,
			data_out	=>	rx_data_out,
			received	=>	rx_received
		);
	
	-- UART Transmitter
	tx:	UARTTransmitter
		port map(
			clk			=>	clock,
			reset		=>	reset,
			enable		=>	'1',
			send		=>	tx_send,
			data_in		=>	tx_data_in,
			data_out	=>	tx_data_out,
			sent		=>	tx_sent
		);
	
	cp: ORBCoprocessor
		GENERIC MAP(
			DESCRIPTOR_LENGTH => 256
		)
		PORT MAP(
			LEDs				=>	LEDs,
			CLOCK				=>	clock,
			DATA_IN				=>	cp_data_in,
			DATA_OUT			=>	cp_data_out,
			IN_FIFO_READ		=>	cp_fifo_rd,
			IN_FIFO_EMPTY		=>	cp_fifo_in_empty,
			OUT_FIFO_FULL		=>	cp_fifo_out_full,
			WRITE_SIGNAL		=>	cp_fifo_wt,
			RESET				=>	reset,
			
			MEM_ADDRESS			=>	cp_mem_addr,
			MEM_READ			=>	cp_mem_rd,
			MEM_WRITE			=>	cp_mem_wt,
			MEM_DATA_IN			=>	cp_mem_data_in,
			MEM_DATA_OUT		=>	cp_mem_data_out,
			MEM_WRITE_FINISHED	=>	cp_mem_write_finished,
			MEM_READ_FINISHED	=>	cp_mem_read_finished
		);
	
	sram_control	:	SRAMController
		port map(
			clock			=>	clock,
			reset			=>	reset,
			data_in			=>	sram_data_in,
			data_out		=>	sram_data_out,
			-- c				=>	,
			sram_write		=>	sram_wt,
			sram_read		=>	sram_rd,
			read_finished	=>	sram_read_finished,
			write_finished	=>	sram_write_finished,
			address			=>	sram_address,
			ce_n			=>	ce,
			we_n			=>	we,
			oe_n			=>	oe,
			adv_n			=>	adv,
			-- cre				=>	,
			dq				=>	dq,
			lb_n			=>	lb,
			ub_n			=>	ub,
			busy			=>	w,
			out_address		=>	addr
		);
	
	-- this might move to a transmitter controller
	clk_proc	:	process(clock, reset)
	begin
	
		if (reset = '1') then
		
			send	<=	'0';
			prev_send	<=	'0';
			tx_prev_sent	<=	'0';
			tx_prev_send	<=	'0';
	
		elsif (rising_edge(clock)) then
		
			if (fifo_tx_empty = '0') then
				send	<=	'1';
			elsif (tx_sent = '1') then
				send	<=	'0';
			end if;
			prev_send		<=	send;
			tx_prev_sent	<= tx_sent;
			tx_prev_send	<= tx_send;
		
		end if;
	
	end process;

end arch;