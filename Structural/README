Alright, so (subject to change) I'm thinking that we can reuse the top level module from project 5/6, with the UART receiver, input fifo, coprocessor, output fifo, UART transmitter 
set up. We can just replace the coprocessor part with the new ORBCoprocessor.vhd module. As far as the new ORBCoprocessor, we should probably start from scratch with that, 
with the exception of the SRAM controller (mine works). I think the more modular we make the coprocessor, the less of a pain in the ass it will become when our state machine gets enormous. 
Also, we should test everything independently. I was thinking the following modules:

Summator: Will perform the summation to find the intensity centroid. We can feed in pixel data as we receive it from the PC to minimize the overhead. It will be sequential.
(DONE, NEEDS TESTING)
entity Summator is 
	port(
		CLOCK : in std_logic;
		ENABLE : in std_logic;
		DATA_IN : in std_logic_vector( 8 downto 1 );
		x : in std_logic_vector( 8 downto 1 );
		y : in std_logic_vector( 8 downto 1 );
		m_00 : out std_logic_vector( 32 downto 1 );
		m_01 : out std_logic_vector( 32 downto 1 );
		m_10 : out std_logic_vector( 32 downto 1 )
	);
end entity;

WidePixelComparator: Will take an array of pixel values and form the final descriptor out of them. Will be combinatorial.
(DONE, NEEDS TESTING)
-- somewhere in a package
type pixel_column is array( integer range <> ) of std_logic_vector( 8 downto 1 );
-- later on
entity WidePixelComparator is
	port(
		DATA_IN_LEFT : in pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_IN_RIGHT : in pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_OUT : out std_logic_vector( DESCRIPTOR_LENGTH downto 1 )
	);
end entity;

WidePointRotator: Will correct the predetermined points. Consequently, those points will be defined inside this module.
-- somewhere in a package
type point is array( 2 downto 1 ) of std_logic_vector( 8 downto 1 );
type point_column is array( integer range <> ) of point;
-- later on
entity WidePointRotator is
	port(
		CLOCK : in std_logic;
		START : in std_logic;
		m_00 : in std_logic_vector( 32 downto 1 );
		m_01 : in std_logic_vector( 32 downto 1 );
		m_10 : in std_logic_vector( 32 downto 1 );
		FINISHED : out std_logic;
		DATA_OUT_LEFT : out point_column( DESRIPTOR_LENGTH downto 1 );
		DATA_OUT_RIGHT : out point_column( DESCRIPTOR_LENGTH downto 1 )
	);
end entity;

PointExtractor: This entity will abstract away all the control involved in extracting the pixel values from SRAM. I was thinking
we could multiplex the control signals into SRAM between standard coprocessor controls and this module.
entity PointExtractor is
	port(
		CLOCK : in std_logic;
		ENABLE : in std_logic;
		START : in std_logic;
		SRAM_READ_FINISHED : in std_logic;
		SRAM_WRITE_FINISHED : in std_logic;
		SRAM_DATA_OUT : in std_logic_vector( 8 downto 1 );
		DATA_IN_LEFT : in point_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_IN_RIGHT : in point_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_OUT_LEFT : out pixel_column( DESCRIPTOR_LENGTH downto 1 );
		DATA_OUT_RIGHT : out pixel_column( DESCRIPTOR_LENGTH downto 1 );
		SRAM_WRITE : out std_logic;
		SRAM_READ : out std_logic;
		SRAM_ADDRESS : out std_logic_vector( 22 downto 0 );
		FINISHED : out std_logic
	);
end entity;

Some helper modules:

SinglePixelComparator: Used as the basis component for the wide pixel comparator
(DONE, NEEDS TESTING)
entity SinglePixelComparator is
	port(
		DATA_IN_LEFT : in std_logic_vector( 8 downto 1 );
		DATA_IN_RIGHT : in std_logic_vector( 8 downto 1 );
		DATA_OUT : out std_logic
	);
end entity;


SinglePointRotator: Used to correct one point. Not sure exactly how we're going to do this yet, but I'm assuming that we'll multiply by the
normalized centroid (here denoted x_0, y_0). We will want to used fixed point arithmetic, (probably something like 16.16 precision)
enttiy SinglePointRotator is
	port(
		DATA_IN : in point;
		x_0 : in std_logic_vector( 32 downto 1 );
		y_0 : in std_logic_vector( 32 downto 1 );
		DATA_OUT : out point
	);
end entity;

NormalizedCentroidCalculator: This is the component where we'll have to do our most intensive math (this assumes that we're using the normalized
centroid to do point rotation). This and the SinglePointRotator will make up the WidePointRotator.
I think I've worked out most of the control stuff for this, but alas, I'm off to some plans.
entity NormalizedCentroidCalculator is
	port(
		CLOCK : in std_logic;
		START : in std_logic;
		m_00 : in std_logic_vector( 32 downto 1 );
		m_01 : in std_logic_vector( 32 downto 1 );
		m_10 : in std_logic_vector( 32 downto 1 );
		FINISHED : out std_logic;
		x_0 : out std_logic_vector( 32 downto 1 );
		y_0 : out std_logic_vector( 32 downto 1 )
	);
end entity;

Dividor: This component will be used to calculate m_01/m_00, m_10/m_00, centr/magn(centr) and such. If we make it a discrete component, we can accomplish
two things - 1.) Hardware reuse, 2.) Super duper efficiency. We can implement this using CORDIC
(DONE, NEEDS TESTING)
This algorithm turns out to have a lot of problems, we might be better off just using IEEE standard division. Specifically, the result of the division
for this entity must lie between -1 and 1. This is ok for the vector normalization, because the components of the unit vector will be less than or
equal to 1, but for finding the centroid ( m_01/m_00, m_10/m_00 ) it presents an issue.
entity Dividor is
	port(
		CLOCK : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		quotient : out std_logic_vector( 32 downto 1 ) -- X / Y
	);
end entity;

MagnitudeCalculator: This component will find the magnitude of the centroid (sqrt(x^2 + y^2)). According to
the CORDIC people, all we have to do is rotate the centroid point clockwise until it aligns with the X axis,
at which point its X component will be equal to its magnitude times some predictable scalar (approximately equal
to 0.6)
(DONE, WORKS)
entity MagnitudeCalculator is
	port(
		CLOCK : in std_logic;
		START : in std_logic;
		FINISHED : out std_logic;
		X : in std_logic_vector( 32 downto 1 );
		Y : in std_logic_vector( 32 downto 1 );
		MAGNITUDE : out std_logic_vector( 32 downto 1 )
	);
end entity;





