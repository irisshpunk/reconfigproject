clear all;

load( 'random_points.mat' )

for i = 1:256
    fprintf( 'DATA_OUT( %i downto %i ) <= X"%04x";\n', 16*(2*i - 1), 16*(2*i-2) + 1, point_matrix( i, 1 )-1 );
    fprintf( 'DATA_OUT( %i downto %i ) <= X"%04x";\n', 16*(2*i), 16*(2*i-1) + 1, point_matrix( i, 2 )-1 ); 
end

for i = 1:256
    fprintf( 'DATA_OUT( %i downto %i ) <= X"%04x";\n', 16*(2*i - 1) + 256*32, 16*(2*i-2) + 1 + 256*32, point_matrix( i, 3 )-1 );
    fprintf( 'DATA_OUT( %i downto %i ) <= X"%04x";\n', 16*(2*i) + 256*32, 16*(2*i-1) + 1 + 256*32, point_matrix( i, 4 )-1 ); 
end
